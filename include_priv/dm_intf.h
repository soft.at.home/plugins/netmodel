/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__DM_INTF_H__)
#define __DM_INTF_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "netmodel.h"
#include "intf.h"
#include "dm_netmodel.h"


// init/cleanup functions
void init_intf(void);
void cleanup_intf(void);
void init_queries(void);
void cleanup_queries(void);

const char* query_update_reason_to_str(enum query_update_reason reason);
void update_queries(intf_t* intf,
                    const amxc_set_t* set,
                    enum query_update_reason reason);
void update_mibs(amxd_object_t* object);

intf_t* intf_get_priv(amxd_object_t* obj);

// Datamodel functions
amxd_status_t _setFlag(amxd_object_t* object,
                       amxd_function_t* func,
                       amxc_var_t* args,
                       amxc_var_t* ret);

amxd_status_t _clearFlag(amxd_object_t* object,
                         amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret);

amxd_status_t _hasFlag(amxd_object_t* object,
                       amxd_function_t* func,
                       amxc_var_t* args,
                       amxc_var_t* ret);

amxd_status_t _openQuery(amxd_object_t* object,
                         amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret);

amxd_status_t _closeQuery(amxd_object_t* object,
                          amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret);

amxd_status_t _isUp(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret);

amxd_status_t _isLinkedTo(amxd_object_t* object,
                          amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret);

amxd_status_t _getIntfs(amxd_object_t* object,
                        amxd_function_t* func,
                        amxc_var_t* args,
                        amxc_var_t* ret);

amxd_status_t _luckyIntf(amxd_object_t* object,
                         amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret);

amxd_status_t _getParameters(amxd_object_t* object,
                             amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret);

amxd_status_t _getFirstParameter(amxd_object_t* object,
                                 amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);

amxd_status_t _setParameters(amxd_object_t* object,
                             amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret);

amxd_status_t _setFirstParameter(amxd_object_t* object,
                                 amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);

amxd_status_t _getAddrs(amxd_object_t* object,
                        amxd_function_t* func,
                        amxc_var_t* args,
                        amxc_var_t* ret);

amxd_status_t _luckyAddr(amxd_object_t* object,
                         amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret);

amxd_status_t _luckyAddrAddress(amxd_object_t* object,
                                amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret);

amxd_status_t _resolvePath(amxd_object_t* object,
                           amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret);

amxd_status_t _getDHCPOption(amxd_object_t* object,
                             amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret);

amxd_status_t _getMibs(amxd_object_t* object,
                       amxd_function_t* func,
                       amxc_var_t* args,
                       amxc_var_t* ret);

amxd_status_t _sendEvent(amxd_object_t* object,
                         amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret);

amxd_status_t _getIPv6Prefixes(amxd_object_t* object,
                               amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret);

amxd_status_t _getFirstIPv6Prefix(amxd_object_t* object,
                                  amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret);

// Event handlers
void _interface_added(const char* const sig_name,
                      const amxc_var_t* const data,
                      void* const priv);

void _interface_removed(const char* const sig_name,
                        const amxc_var_t* const data,
                        void* const priv);

void _intf_changed(const char* const sig_name,
                   const amxc_var_t* const data,
                   void* const priv);

void _mib_changed(const char* const sig_name,
                  const amxc_var_t* const data,
                  void* const priv);

void _addr_changed(const char* const sig_name,
                   const amxc_var_t* const data,
                   void* const priv);

void _addr6_changed(const char* const sig_name,
                    const amxc_var_t* const data,
                    void* const priv);

void _path_changed(const char* const sig_name,
                   const amxc_var_t* const data,
                   void* const priv);

void _dhcp_changed(const char* const sig_name,
                   const amxc_var_t* const data,
                   void* const priv);

void _prefix_changed(const char* const sig_name,
                     const amxc_var_t* const data,
                     void* const priv);

// Action handlers
amxd_status_t _query_removed(amxd_object_t* object,
                             amxd_param_t* param,
                             amxd_action_t reason,
                             const amxc_var_t* const args,
                             amxc_var_t* const retval,
                             void* priv);

amxd_status_t _interface_instance_added(amxd_object_t* object,
                                        amxd_param_t* param,
                                        amxd_action_t reason,
                                        const amxc_var_t* const args,
                                        amxc_var_t* const retval,
                                        void* priv);
#ifdef __cplusplus
}
#endif

#endif // __DM_INTF_H__
