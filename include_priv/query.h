/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NETMODEL_QUERY_H__
#define __NETMODEL_QUERY_H__

/**
   @defgroup query query.h - Query management
   @{
 */

#include <stdbool.h>

#include "intf.h"

typedef struct _query_class {
    amxc_llist_it_t it;
    amxd_function_t* function;
    amxc_llist_t queries;
} query_class_t;

typedef struct _query {
    amxc_llist_it_t it;        // Iterator for the query class list
    amxc_llist_it_t intf_it;   // Iterator for the intf on which the query is creatd
    unsigned int id;
    amxd_object_t* intf;
    query_class_t* query_class;
    amxc_set_t* subscribers;
    amxc_var_t result;
    amxc_var_t args;
} query_t;

/**
   @brief
   A set of general event listener callback functions for queries.
 */
typedef struct _query_listener {
    /**
       @brief
       A query has been opened.

       @details
       This function is not called when a call to openQuery() resulted in an existing, multiplexed Query object.

       @param query The opened query.
       @param userdata Void pointer passed to query_add_listener()
     */
    void (* opened)(query_t* query, void* userdata);

    /**
       @brief
       A query has been closed.

       @details
       This function is not called when the caller of closeQuery() was not the last subscriber of the query.

       @param query The closed query.
       @param userdata Void pointer passed to query_add_listener()
     */
    void (* closed)(query_t* query, void* userdata);

    /**
       @brief
       The set of subscribers of a query has been changed.

       @param query The altered query.
       @param userdata Void pointer passed to query_add_listener()
     */
    void (* altered)(query_t* query, void* userdata); // set of subscribers changes

    /**
       @brief
       The query result has changed.

       @details
       This is function not always called when the query was recalculated, but only if the result really changed.

       @param query The triggered query.
       @param userdata Void pointer passed to query_add_listener()
     */
    void (* triggered)(query_t* query, void* userdata); // query result has changed
} query_listener_t;

/**
   @brief
   Register a query class.

   @param function The name of the function to be executed for this query class. This function must be defined in the data model.
   @return An opaque object holding the query class. It should be released again using query_class_unregister().
 */
query_class_t* query_class_register(const char* function);

/**
   @brief
   Unregister a query class.

   @param cl The query class to unregister.
 */
void query_class_unregister(query_class_t* cl);

/**
   @brief
   Find a query class by name.

   @param name Name of the query class. This should equal the name of the corresponding function with query support, e.g. "isUp".
   @return The query class if it was found. NULL otherwise.
 */
query_class_t* query_class_find(const char* name);

/**
   @brief
   Get the function object for a query class

   @param cl Pointer to the query class
   @return The function object if cl is not NULL, NULL otherwise
 */
amxd_function_t* query_class_function(const query_class_t* cl);

/**
   @brief
   Start iterating over all registered query classes.

   @return The first registered query class.
 */
query_class_t* query_class_first(void);

/**
   @brief
   Iterate over all registered query classes.

   @param cl The current registered query class.
   @return The next registered query class.
 */
query_class_t* query_class_next(query_class_t* cl);

/**
   @brief
   Open a query of the specified query class.

           external usage.
   @param subscriber An arbitrary subscriber name.
   @param intf The Intf to open the query on.
   @param cl The query class to open a query of.
   @param args The arguments to be passed to the query function.
   @return A query object if opening the query succeeded. This object is to be released with query_destroy() or with a subsequent
        call to query_close() with a matching set of arguments. Notice that the returned query pointer might point to a newly
        created query object or to an existing query object if the arguments match any previously opened query. If opening the
        query failed, a NULL pointer is returned.
 */
query_t* query_open(const char* subscriber,
                    query_class_t* cl,
                    amxd_object_t* intf,
                    amxc_var_t* args);

/**
   @brief
   Close a query given its arguments.

   @param subscriber An arbitrary subscriber name.
   @param intf The Intf to close a query on.
   @param cl The query class to close a query of.
   @param args The arguments to be passed to the query function.
   @return The ID of the query that was closed, or 0 in case the query was not found
   @remark
   This function might destroy a query object, but only if there was a matching query object found for which the provided subscriber was the last one.
 */
unsigned int query_close(const char* subscriber,
                         query_class_t* cl,
                         amxd_object_t* intf,
                         amxc_var_t* args);
/**
   @brief
   Brutely destroy a query object.

   @param q The query to destroy.
 */
void query_destroy(query_t* q);

/**
   @brief
   Start iterating over the opened queries of a specific query class.

   @param cl The query class to start iterating over.
   @return The first query of the query class.
 */
query_t* query_first(query_class_t* cl);

/**
   @brief
   Iterate over the opened queries of a specific query class.

   @param q The current query.
   @return The next query of the query class.
 */
query_t* query_next(query_t* q);

/**
   @brief
   Get the ID of a query.

   @details
   This equals the key of the corresponding NetModel.Intf.{i}.Query.{j} instance.

   @param q The query to get the ID of.
   @return The non-zero query ID.
 */
unsigned int query_id(query_t* q);

/**
   @brief
   Retrieve a query object by it's ID.

   @param id The ID to retrieve the query object for.
   @return The query object if found, NULL otherwise.
 */
query_t* query_find(unsigned int id);

/**
   @brief
   Get a human readable description of the query.

   @param q The query to get a description of.
   @return A human readable description of the query, allocated on the heap. It should be released by the caller with free().
        The returned string looks like a pcb_cli command in dot-separated key path notation, e.g.
        NetModel.Intf.x.isUp(flag=y, traverse=down).
 */
char* query_describe(query_t* q);

/**
   @brief
   Get the set of subscribers of a query.

   @param q The query to get the subscribers of.
   @return The set of subscribers modeled as a recursive flag set. This is a pointer to an internal structure, please consider it to
        be read-only.
 */
amxc_set_t* query_subscribers(query_t* q);

/**
   @brief
   Get the current cached query result

   @param q Query to get the result from.
   @return The current cached query result.
 */
const amxc_var_t* query_result(query_t* q);

/**
   @brief
   Get the binary arguments object of a query.

   @param q Query to get the binary arguments object from.
   @return The binary arguments object of the query.
 */
const amxc_var_t* query_args(query_t* q);

/**
   @brief
   Get the Intf object to which a query applies.

   @param q The query to get the Intf from.
   @return The Intf object to which the query applies.
 */
amxd_object_t* query_intf(query_t* q);

/**
   @brief
   Get the query class of which a query is an instance.

   @param q The query instance to get the class of.
   @return The query class of which the query is an instance.
 */
query_class_t* query_class(query_t* q);

/**
   @brief
   Trigger the recalculation of a query.

   @details
   Queries are never recalculated automatically by the generic query management component. The triggering of queries completely relies
   on this function getting called from within the query class implementation every time a query result potentially changed. Notice
   that queries should be invalidated as little as possible to minimize the CPU time consumption (query recalculations contribute
   significantly to the total amount of NetModel's CPU time consumption), ideally only when the query result has changed for sure, but
   certainly not less than that. False positives (recalculating the query result while it hasn't changed) cause less optimal
   performance, but false negatives (not recalculating the query result while it changed) break the query mechanism severely.

   @param q The query to be invalidated.
 */
void query_invalidate(query_t* q);


/**
   @brief
   Register a general query listener.

   @param l The set of callback functions to be called upon different events.
   @param userdata A void pointer to pass to the callback functions.
 */
void query_add_listener(query_listener_t* l, void* userdata);

/**
   @brief
   Unregister a general query listener.

   @param l Should match the l argument of the corresponding call to query_add_listener() exactly.
   @param userdata Should match the userdata argument of the corresponding call to query_add_listener() exactly.
 */
void query_del_listener(query_listener_t* l, void* userdata);

/**
   @}
 */

#endif // __NETMODEL_QUERY_H__
