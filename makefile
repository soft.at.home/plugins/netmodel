include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C odl all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C odl clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -d -m 0755 $(DEST)//usr/lib/amx/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/*.so $(DEST)/usr/lib/amx/$(COMPONENT)/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
ifeq ($(CONFIG_SAH_AMX_NETMODEL_DIRECT_SOCKET),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_direct_socket.odl $(DEST)/etc/amx/$(COMPONENT)/extensions/$(COMPONENT)_direct_socket.odl
endif
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/interface_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)/interface_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/interface_functions.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)/interface_functions.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/query_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)/query_definition.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(foreach odl,$(wildcard odl/$(COMPONENT)_defaults/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/;)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/mibs
	$(foreach odl,$(wildcard mibs/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/mibs/;)
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -d -m 0755 $(PKGDIR)//usr/lib/amx/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/*.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
ifeq ($(CONFIG_SAH_AMX_NETMODEL_DIRECT_SOCKET),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_direct_socket.odl $(PKGDIR)/etc/amx/$(COMPONENT)/extensions/$(COMPONENT)_direct_socket.odl
endif
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/interface_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)/interface_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/interface_functions.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)/interface_functions.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/query_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)/query_definition.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/mibs
	$(INSTALL) -D -p -m 0644 mibs/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/mibs/
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	mkdir -p output/doc
	VERSION=$(VERSION) doxygen doc/netmodel.doxy

	$(eval ODLFILES += odl/$(COMPONENT)_definition.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_caps.odl)
	$(eval ODLFILES += odl/$(COMPONENT)/interface_definition.odl)
	$(eval ODLFILES += odl/$(COMPONENT)/interface_functions.odl)
	$(eval ODLFILES += odl/$(COMPONENT)/query_definition.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test