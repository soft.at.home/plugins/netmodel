/*expr: ip */
%define
{
    /**
     * MIB is loaded on all network devices that have (or had) an ip address
     *
     * All devices matching expression: "ip" are extended with this MIB
     *
     * @version 1.0
     */
    mib ip {
        /**
         * This parameter should be set with the Device path to the LAN interface where the IPv6 address should be taken from in unnumbered mode.
         *
         * @version 1.0
         */
        string IPv6AddressDelegate {
            on action validate call check_maximum_length 1024;
            on action validate call matches_regexp "^Device\.IP\.Interface\.[0-9]+\.$";
        }

        /**
         * @version 1.0
         */
        uint32 MTU;

        /**
         * @version 1.0
         */
        object IPv4Address[] {
            /**
             * Enables or disables this IPv4 address.
             *
             * @version 1.0
             */
            bool Enable = false;

            /**
             * The status of this IPv4Address table entry.
             *
             * @version 1.0
             */
            string Status = "Disabled" {
                on action validate call check_enum ["Disabled", "Enabled", "Error_Misconfigured", "Error"];
            }

            /**
             * A non-volatile unique key used to reference this instance.
             *
             * @version 1.0
             */
            %unique %key string Alias {
                on action validate call check_maximum_length 64;
            }

            /**
             * IPv4 address.
             *
             * @version 1.0
             */
            string IPAddress = "";

            /**
             * Subnet mask.
             *
             * @version 1.0
             */
            string SubnetMask = "";

            /**
             * @version 1.0
             */
            string Scope = "";

            /**
             * @version 1.0
             */
            string Flags = "";

            /**
             * @version 1.0
             */
            string TypeFlags = "";

            /**
             * @version 1.0
             */
            string Peer = "";

            /**
             * @version 1.0
             */
            uint8 PrefixLen = 0;
        }

        /**
         * @version 1.0
         */
        object IPv6Address[] {
            /**
             * Enables or disables this IPv6Address entry.
             *
             * @version 1.0
             */
            bool Enable = false;

            /**
             * The status of this IPv6Address table entry.
             *
             * @version 1.0
             */
            string Status = "Disabled" {
                on action validate call check_enum ["Disabled", "Enabled", "Error_Misconfigured" , "Error"];
            }

            /**
             * A non-volatile unique key used to reference this instance.
             *
             * @version 1.0
             */
            %unique %key string Alias {
                on action validate call check_maximum_length 64;
            }

            /**
             * IPv6 address.
             *
             * @version 1.0
             */
            string IPAddress = "";

            /**
             * IPv6 address prefix.
             *
             * @version 1.0
             */
            string Prefix = "";

            /**
             * @version 1.0
             */
            string Scope = "";

            /**
             * @version 1.0
             */
            string Flags = "";

            /**
             * @version 1.0
             */
            string TypeFlags = "";

            /**
             * @version 1.0
             */
            string Peer = "";

            /**
             * @version 1.0
             */
            uint8 PrefixLen = 0;
        }

        /**
         * @version 1.0
         */
        object IPv6Prefix[] {
            /**
             * Enables or disables this IPv6Prefix entry.
             *
             * @version 1.0
             */
            bool Enable = false;

            /**
             * The status of this IPv6Address table entry.
             *
             * @version 1.0
             */
            string Status = "Disabled" {
                on action validate call check_enum ["Disabled", "Enabled", "Error"];
            }

            /**
             * A non-volatile unique key used to reference this instance.
             *
             * @version 1.0
             */
            %unique %key string Alias {
                on action validate call check_maximum_length 64;
            }

            /**
             * IPv6 prefix.
             *
             * @version 1.0
             */
            string Prefix = "";

            /**
             * Origin of the IPv6 prefix
             *
             * @version 1.0
             */
            string Origin = "";

            /**
             * @version 1.0
             */
            string StaticType = "";

            /**
             * @version 1.0
             */
            string PrefixStatus = "";

            /**
             * @version 1.0
             */
            datetime ValidLifetime = "9999-12-31T23:59:59Z";

            /**
             * @version 1.0
             */
            datetime PreferredLifetime = "9999-12-31T23:59:59Z";

            /**
             * @version 1.0
             */
            uint32 RelativeValidLifetime = 0;

            /**
             * @version 1.0
             */
            uint32 RelativePreferredLifetime = 0;

            /**
             * @version 1.0
             */
            bool Autonomous = 0;

            /**
             * @version 1.0
             */
            bool OnLink = 0;
        }
    }
}
