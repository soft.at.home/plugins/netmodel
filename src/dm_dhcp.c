/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <v4v6option.h>
#include <raoption.h>

#include "intf.h"
#include "traverse.h"
#include "arg.h"
#include "dm_intf.h"

#define ME "netmodel"

typedef enum _type {
    TYPE_REQ = 0,
    TYPE_SENT = 1,
    TYPE_REQ6 = 2,
    TYPE_SENT6 = 3,
    TYPE_RA = 4,
} type_t;

typedef struct _option_type {
    const char* obj_name;
    const char* type_name;
    type_t type;
} option_type_t;

static option_type_t option_types[] = {
    {"ReqOption", "req", TYPE_REQ},
    {"SentOption", "sent", TYPE_SENT},
    {"ReceivedOption", "req6", TYPE_REQ6},
    {"SentOption", "sent6", TYPE_SENT6},
    {"IPv6Router.Option", "ra", TYPE_RA},
};

typedef struct _intf_dhcp_ctx {
    amxc_var_t var;
    uint16_t tag;
    option_type_t* type;
} intf_dhcp_ctx_t;

static option_type_t* get_option_type_by_name(const char* name) {
    unsigned int i = 0;
    option_type_t* ret = NULL;

    for(i = 0; i < sizeof(option_types) / sizeof(option_types[0]); i++) {
        if(strcmp(name, option_types[i].type_name) == 0) {
            ret = &option_types[i];
            break;
        }
    }

    return ret;
}

static amxd_status_t execute_intf_dhcp_function(amxd_object_t* object,
                                                amxc_var_t* args,
                                                bool (* step)(intf_t* intf, void* userdata),
                                                intf_dhcp_ctx_t* ctx) {
    amxd_status_t status = amxd_status_unknown_error;
    traverse_mode_t mode;
    intf_t* intf = (intf_t*) object->priv;
    traverse_tree_t* tree = NULL;
    const char* type_name = NULL;

    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to get interface");
        status = amxd_status_parameter_not_found;
        goto exit;
    }
    when_false(arg_get_traverse(&mode, args, "traverse", &status), exit);
    when_false(arg_get_uint16(&ctx->tag, args, "tag", &status), exit);
    when_false(arg_get_string(&type_name, args, "type", &status), exit);
    ctx->type = get_option_type_by_name(type_name);
    when_null_status(ctx->type, exit, status = amxd_status_invalid_arg);

    tree = traverse_tree_create(mode, intf);
    if(tree == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to create traverse tree");
        goto exit;
    }

    traverse_tree_walk(tree, step, ctx);
    traverse_tree_destroy(tree);

    status = amxd_status_ok;

exit:
    return status;
}

static int get_dhcp_info(amxc_var_t* info,
                         amxd_object_t* inst,
                         intf_dhcp_ctx_t* ctx) {
    int ret = -1;
    const char* hex_str = NULL;
    uint32_t len = 0;
    unsigned char* binary = NULL;

    if(amxd_object_get_value(uint16_t, inst, "Tag", NULL) == ctx->tag) {
        hex_str = amxc_var_constcast(cstring_t, amxd_object_get_param_value(inst, "Value"));
        when_str_empty(hex_str, exit);
        binary = dhcpoption_option_convert2bin(hex_str, &len);
        when_null(binary, exit);

        ret = 0;

        switch(ctx->type->type) {
        case TYPE_REQ:
        case TYPE_SENT:
            dhcpoption_v4parse(info, ctx->tag, len, binary);
            break;
        case TYPE_REQ6:
            dhcpoption_v6parse(info, ctx->tag, len, binary);
            if((info->type_id == AMXC_VAR_ID_HTABLE) && (GET_ARG(info, "T2") != NULL)) {
                char* last_change = amxd_object_get_value(cstring_t, inst, "LastUpdate", NULL);
                amxc_var_t* tmp = amxc_var_add_new_key(info, "LastUpdate");
                amxc_var_push(cstring_t, tmp, last_change);
            }
            break;
        case TYPE_SENT6:
            dhcpoption_v6parse(info, ctx->tag, len, binary);
            break;
        case TYPE_RA:
            raoption_parse(info, ctx->tag, binary);
            if((info->type_id == AMXC_VAR_ID_HTABLE) && (GET_ARG(info, "ValidLifetime") != NULL)) {
                char* last_change = amxd_object_get_value(cstring_t, inst, "LastUpdate", NULL);
                amxc_var_t* tmp = amxc_var_add_new_key(info, "LastUpdate");
                amxc_var_push(cstring_t, tmp, last_change);
            }
            break;
        default:
            SAH_TRACEZ_ERROR(ME, "Unknown dhcp option type: %d", ctx->type->type);
            ret = -1;
            break;
        }
    }

exit:
    free(binary);
    return ret;
}

static bool get_dhcp_step(intf_t* intf, void* data) {
    intf_dhcp_ctx_t* ctx = (intf_dhcp_ctx_t*) data;
    amxd_object_t* obj = NULL;
    amxd_object_t* dhcp_obj = NULL;
    amxc_var_t list;
    bool ret = false;
    bool multi = ((ctx->type->type & 2) != 0) && dhcpoption_v6allowMultiple(ctx->tag);

    amxc_var_init(&list);
    if(multi) {
        amxc_var_set_type(&list, AMXC_VAR_ID_LIST);
    }

    obj = intf_object(intf);
    when_null(obj, exit);

    dhcp_obj = amxd_object_findf(obj, "%s.", ctx->type->obj_name);
    when_null(dhcp_obj, exit);

    amxd_object_iterate(instance, it, dhcp_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        amxc_var_t info;

        amxc_var_init(&info);
        if(get_dhcp_info(&info, instance, ctx) == 0) {
            ret = true;
            if(multi) {
                amxc_var_t* tmp = amxc_var_add_new(&list);
                amxc_var_move(tmp, &info);
                amxc_var_clean(&info);
            } else {
                amxc_var_move(&ctx->var, &info);
                amxc_var_clean(&info);
                break;
            }
        }
    }

    if(multi) {
        amxc_var_move(&ctx->var, &list);
    }

exit:
    amxc_var_clean(&list);
    return ret;
}

amxd_status_t _getDHCPOption(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_dhcp_ctx_t ctx;

    amxc_var_init(&ctx.var);

    status = execute_intf_dhcp_function(object, args, get_dhcp_step, &ctx);
    when_failed(status, exit);

    amxc_var_move(ret, &ctx.var);

exit:
    amxc_var_clean(&ctx.var);
    return status;
}
