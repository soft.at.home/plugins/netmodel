/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "intf.h"
#include "traverse.h"
#include "arg.h"
#include "dm_intf.h"

#define ME "netmodel"

enum link_layer {
    UPPER,
    LOWER
};

enum link_action {
    LINK,
    UNLINK
};

static void update_link_intf(amxd_trans_t* trans,
                             const char* obj_path,
                             const char* me,
                             const char* target,
                             bool added) {
    amxd_object_t* object = NULL;
    amxd_object_t* instance = NULL;

    object = amxd_dm_findf(nm_get_dm(), obj_path, me);
    instance = amxd_object_get(object, target);
    if(added) {
        if(instance == NULL) {
            amxd_trans_select_object(trans, object);
            amxd_trans_add_inst(trans, 0, target);
            amxd_trans_set_value(cstring_t, trans, "Name", target);
        }
    } else {
        if(instance != NULL) {
            amxd_trans_select_object(trans, object);
            amxd_trans_del_inst(trans, 0, target);
        }
    }
}

static void param_to_var(amxd_object_t* obj,
                         const char* const param_name,
                         const char* const key,
                         amxc_var_t* var) {
    amxc_var_t* param = amxc_var_add_new_key(var, key);
    amxd_object_get_param(obj, param_name, param);
}

static int fill_event_data(const char* const ulintf,
                           const char* const llintf,
                           amxc_var_t* data) {
    int ret = -1;
    amxd_object_t* obj = NULL;

    obj = amxd_dm_findf(nm_get_dm(), "NetModel.Intf.%s.", ulintf);
    when_null(obj, exit);

    param_to_var(obj, "InterfacePath", "HigherLayer", data);
    param_to_var(obj, "InterfaceAlias", "HigherAlias", data);

    obj = amxd_dm_findf(nm_get_dm(), "NetModel.Intf.%s.", llintf);
    when_null(obj, exit);

    param_to_var(obj, "InterfacePath", "LowerLayer", data);
    param_to_var(obj, "InterfaceAlias", "LowerAlias", data);

    ret = 0;

exit:
    return ret;
}

static void send_link_event(const char* const ulintf,
                            const char* const llintf,
                            const char* const signal) {
    amxc_var_t data;
    amxd_object_t* obj = amxd_dm_findf(nm_get_dm(), "NetModel.");

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    when_null(obj, exit);
    when_failed(fill_event_data(ulintf, llintf, &data), exit);

    amxd_object_emit_signal(obj, signal, &data);

exit:
    amxc_var_clean(&data);
    return;
}

static void intf_link_handler(intf_t* ulintf, intf_t* llintf, bool value, UNUSED void* userdata) {
    amxd_trans_t trans;
    amxc_set_t set;
    amxd_status_t status = amxd_status_unknown_error;
    const char* ulintf_str = intf_name(ulintf);
    const char* llintf_str = intf_name(llintf);

    SAH_TRACEZ_INFO(ME, "%s dependency %s -> %s", value ? "Added" : "Removed", ulintf_str, llintf_str);
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxc_set_init(&set, false);

    update_link_intf(&trans, "NetModel.Intf.%s.LLIntf.", ulintf_str, llintf_str, value);
    update_link_intf(&trans, "NetModel.Intf.%s.ULIntf.", llintf_str, ulintf_str, value);

    status = amxd_trans_apply(&trans, nm_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to update upper/lower layer interfaces");
    }

    send_link_event(ulintf_str, llintf_str, value ? "nm:link" : "nm:unlink");

    amxc_set_parse(&set, llintf_str);
    update_queries(ulintf, &set, REASON_LINK);

    amxd_trans_clean(&trans);
    amxc_set_clean(&set);
}

static void intf_update_flag(amxd_object_t* obj, const char* flag, bool add) {
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", flag);
    amxc_var_add_key(cstring_t, &args, "condition", "");
    amxc_var_add_key(cstring_t, &args, "traverse", "this");

    if(add) {
        _setFlag(obj, NULL, &args, &ret);
    } else {
        _clearFlag(obj, NULL, &args, &ret);
    }

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

static amxp_expr_status_t nm_custom_field_fetcher(amxp_expr_t* expr,
                                                  amxc_var_t* value,
                                                  const char* path,
                                                  void* priv) {
    amxp_expr_status_t status = amxp_expr_status_unknown_error;
    amxd_object_t* intf_obj = (amxd_object_t*) priv;
    intf_t* intf = intf_get_priv(intf_obj);

    if(path[0] == '.') {
        // Field is object path.
        status = amxd_object_expr_get_field(expr, value, path, intf_obj);
    } else {
        // Field is flag.
        amxc_var_set(bool, value, amxc_set_has_flag(intf_flagset(intf), path));
        status = amxp_expr_status_ok;
    }

    return status;
}

static bool nm_intf_matches(amxd_object_t* intf,
                            amxp_expr_t* expression) {
    return amxp_expr_evaluate(expression, nm_custom_field_fetcher, intf, NULL);
}

void init_intf(void) {
    intf_add_linklistener(NULL, NULL, intf_link_handler, NULL, true);
}

void cleanup_intf(void) {
    intf_del_linklistener(NULL, NULL, intf_link_handler, NULL, true);
    while(intf_first() != NULL) {
        intf_destroy(intf_first());
    }
}

static intf_t* intf_init_priv_data(const char* name, const char* flags, amxd_object_t* obj) {
    intf_t* intf = NULL;

    when_str_empty(name, exit);
    when_null(obj, exit);

    intf = intf_find(name);
    if(intf == NULL) {
        intf = intf_create(name);
    }
    when_null(intf, exit);

    intf_object_set(intf, obj);
    obj->priv = intf;

    if(flags != NULL) {
        amxc_set_parse(intf_flagset(intf), flags);
    }

    update_mibs(obj);

exit:
    return intf;
}

intf_t* intf_get_priv(amxd_object_t* obj) {
    intf_t* intf = NULL;

    when_null(obj, exit);

    intf = (intf_t*) obj->priv;
    if(intf == NULL) {
        const char* name = amxd_object_get_name(obj, AMXD_OBJECT_NAMED);
        const char* flags = amxc_var_constcast(cstring_t, amxd_object_get_param_value(obj, "Flags"));
        intf = intf_init_priv_data(name, flags, obj);
    }

exit:
    return intf;
}

void _interface_added(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv) {
    intf_t* intf = intf_find(GET_CHAR(data, "name"));
    amxd_object_t* obj = amxd_dm_signal_get_object(nm_get_dm(), data);
    amxd_object_t* inst = amxd_object_get_instance(obj, NULL, GET_UINT32(data, "index"));
    amxc_set_t set;

    amxc_set_init(&set, false);

    when_null_trace(inst, exit, ERROR, "Could not find Interface instance");
    when_null_trace(intf, exit, ERROR, "Could not find interface with name %s", GET_CHAR(data, "name"));

    // If an Interface is created because it is defined in an odl file,
    // the flagset will be empty. This is due to the fact that the Flags parameter
    // is not accessible in the instance-added action handler.
    // Add the contents of the Flags parameter to the flagset here to fix that.
    amxc_set_parse(&set, GETP_CHAR(data, "parameters.Flags"));
    amxc_set_union(intf_flagset(intf), &set);

    update_mibs(inst);
    update_queries(NULL, NULL, REASON_INTF);

exit:
    amxc_set_clean(&set);
    return;
}

amxd_status_t _interface_instance_added(amxd_object_t* object,
                                        amxd_param_t* param,
                                        amxd_action_t reason,
                                        const amxc_var_t* const args,
                                        amxc_var_t* const retval,
                                        void* priv) {
    intf_t* intf = NULL;
    amxd_object_t* inst = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    when_false_trace(reason == action_object_add_inst, exit, ERROR, "Not an instance added action");

    status = amxd_action_object_add_inst(object, param, reason, args, retval, priv);
    when_failed_trace(status, exit, ERROR, "Failed to create interface instance");

    inst = amxd_object_get(object, GET_CHAR(args, "name"));
    when_null_trace(inst, exit, ERROR, "Could not find instance");

    intf = intf_get_priv(inst);
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to create intf private data");
    }

exit:
    return status;
}

void _interface_removed(UNUSED const char* const sig_name,
                        const amxc_var_t* const data,
                        UNUSED void* const priv) {
    const char* name = NULL;
    intf_t* intf = NULL;

    name = GET_CHAR(data, "name");
    intf = intf_find(name);
    if(intf != NULL) {
        intf_object_set(intf, NULL);
        intf_destroy(intf);
    }

    update_queries(NULL, NULL, REASON_INTF);
}

static void intf_flags_changed(const amxc_var_t* const data) {
    amxc_set_t from;
    amxc_set_t to;
    amxd_object_t* object = amxd_dm_signal_get_object(nm_get_dm(), data);
    intf_t* intf = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    intf = (intf_t*) object->priv;
    when_null_trace(intf, exit, ERROR, "Intf is NULL");

    // Update mibs
    update_mibs(object);

    // Get the differences between to and from
    amxc_set_init(&from, false);
    amxc_set_init(&to, false);
    amxc_set_parse(&from, GETP_CHAR(data, "parameters.Flags.from"));
    amxc_set_parse(&to, GETP_CHAR(data, "parameters.Flags.to"));
    amxc_set_symmetric_difference(&from, &to);

    update_queries(intf, &from, REASON_FLAG);

    amxc_set_clean(&to);
    amxc_set_clean(&from);

exit:
    return;
}

static void intf_status_changed(const amxc_var_t* const data) {
    amxd_object_t* obj = amxd_dm_signal_get_object(nm_get_dm(), data);
    bool status = GETP_BOOL(data, "parameters.Status.to");

    intf_update_flag(obj, "up", status);
}

static void intf_status_ext_changed(const amxc_var_t* const data) {
    amxd_object_t* obj = amxd_dm_signal_get_object(nm_get_dm(), data);
    const char* status_ext = GETP_CHAR(data, "parameters.Status_ext.to");
    bool up = false;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_str_empty(status_ext, exit);

    up = strcmp(status_ext, "Up") == 0;
    intf_update_flag(obj, "up", up);

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(bool, &trans, "Status", up);
    when_failed_trace(amxd_trans_apply(&trans, nm_get_dm()), exit, ERROR,
                      "Failed to set Status parameter");

exit:
    amxd_trans_clean(&trans);
    return;
}

static void intf_enable_changed(const amxc_var_t* const data) {
    amxd_object_t* obj = amxd_dm_signal_get_object(nm_get_dm(), data);
    bool enable = GETP_BOOL(data, "parameters.Enable.to");

    intf_update_flag(obj, "enabled", enable);
}

void _intf_changed(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* event_obj = amxd_dm_signal_get_object(nm_get_dm(), data);
    amxd_object_t* intf_obj = amxd_dm_findf(nm_get_dm(), "NetModel.Intf.");
    amxd_object_t* obj = NULL;
    amxc_var_t* params = GET_ARG(data, "parameters");
    amxc_string_t str;
    amxc_set_t set;
    intf_t* intf = NULL;

    amxc_string_init(&str, 0);
    amxc_set_init(&set, false);

    when_null(params, exit);
    when_null(intf_obj, exit);
    when_null(event_obj, exit);

    if(amxd_object_get_parent(event_obj) == intf_obj) {
        if(GET_ARG(params, "Flags") != NULL) {
            intf_flags_changed(data);
        }
        if(GET_ARG(params, "Status") != NULL) {
            intf_status_changed(data);
        }
        if(GET_ARG(params, "Status_ext") != NULL) {
            intf_status_ext_changed(data);
        }
        if(GET_ARG(params, "Enable") != NULL) {
            intf_enable_changed(data);
        }
        if(GET_ARG(params, "InterfacePath") != NULL) {
            _path_changed(sig_name, data, priv);
        }
    }

    amxc_var_for_each(param, params) {
        amxc_string_set(&str, amxc_var_key(param));
        obj = event_obj;
        while(amxd_object_get_parent(obj) != intf_obj) {
            amxc_string_prependf(&str, "%s.", amxd_object_get_name(obj, AMXD_OBJECT_NAMED));
            obj = amxd_object_get_parent(obj);
        }
        amxc_set_add_flag(&set, amxc_string_get(&str, 0));
    }

    // It is possible that this event is sent by a subobject of a NetModel.Intf. instance
    // Find the NetModel.Intf. instance object, the correct intf_t structure is linked to this
    obj = event_obj;
    while(amxd_object_get_parent(obj) != intf_obj) {
        obj = amxd_object_get_parent(obj);
    }
    intf = (intf_t*) obj->priv;
    when_null_trace(intf, exit, ERROR, "Intf is NULL");

    update_queries(intf, &set, REASON_PARAM);

exit:
    amxc_string_clean(&str);
    amxc_set_clean(&set);
}

static intf_t* get_intf_from_event_data(const amxc_var_t* data) {
    amxd_object_t* obj = amxd_dm_signal_get_object(nm_get_dm(), data);
    amxd_object_t* intf_obj = amxd_dm_findf(nm_get_dm(), "NetModel.Intf.");
    intf_t* intf = NULL;

    when_null_trace(obj, exit, ERROR, "Object is NULL");
    when_null_trace(intf_obj, exit, ERROR, "Intf_object is NULL");

    // It is possible that this event is sent by a subobject of a NetModel.Intf. instance
    // Find the NetModel.Intf. instance object, the correct intf_t structure is linked to this
    while(amxd_object_get_parent(obj) != intf_obj) {
        obj = amxd_object_get_parent(obj);
    }

    intf = (intf_t*) obj->priv;
    when_null_trace(intf, exit, ERROR, "Intf is NULL");

exit:
    return intf;
}

void _mib_changed(UNUSED const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv) {
    intf_t* intf = get_intf_from_event_data(data);
    when_null(intf, exit);

    update_queries(intf, NULL, REASON_MIB);

exit:
    return;
}

void _addr_changed(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    intf_t* intf = get_intf_from_event_data(data);
    when_null(intf, exit);

    update_queries(intf, NULL, REASON_ADDR);

exit:
    return;
}

void _addr6_changed(UNUSED const char* const sig_name,
                    const amxc_var_t* const data,
                    UNUSED void* const priv) {
    intf_t* intf = get_intf_from_event_data(data);
    amxc_set_t set;

    amxc_set_init(&set, false);
    when_null(intf, exit);

    amxc_set_add_flag(&set, "ipv6");

    update_queries(intf, &set, REASON_ADDR);

exit:
    amxc_set_clean(&set);
    return;
}
void _path_changed(const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxc_set_t set;
    amxc_set_init(&set, false);

    when_str_empty_trace(sig_name, exit, ERROR, "Event has no valid signal name");

    if(strcmp(sig_name, "dm:instance-added") == 0) {
        amxc_set_parse(&set, GETP_CHAR(data, "parameters.InterfacePath"));
    } else {
        amxc_set_parse(&set, GETP_CHAR(data, "parameters.InterfacePath.from"));
        amxc_set_add_flag(&set, GETP_CHAR(data, "parameters.InterfacePath.to"));
    }

    update_queries(NULL, &set, REASON_PATH);

exit:
    amxc_set_clean(&set);
    return;
}

static const char* dhcp_obj_name_to_type(const char* name) {
    const char* type = "invalid";

    when_null(name, exit);

    if(strcmp(name, "ReqOption") == 0) {
        type = "req";
    } else if(strcmp(name, "ReceivedOption") == 0) {
        type = "req6";
    } else if(strcmp(name, "Option") == 0) {
        type = "ra";
    }

exit:
    return type;
}

void _dhcp_changed(const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(nm_get_dm(), data);
    intf_t* intf = get_intf_from_event_data(data);
    const amxc_var_t* tag = NULL;
    const char* type = NULL;
    amxc_set_t set;
    char* tag_str = NULL;

    amxc_set_init(&set, false);

    when_str_empty_trace(sig_name, exit, ERROR, "Signal name is empty");
    when_null_trace(obj, exit, ERROR, "Object is NULL");
    when_null_trace(intf, exit, ERROR, "Intf is NULL");

    if(strcmp(sig_name, "dm:object-changed") == 0) {
        // This is an instance object
        tag = amxd_object_get_param_value(obj, "Tag");
        type = amxd_object_get_name(amxd_object_get_parent(obj), AMXD_OBJECT_NAMED);
    } else {
        // This is a template object
        tag = GETP_ARG(data, "parameters.Tag");
        type = amxd_object_get_name(obj, AMXD_OBJECT_NAMED);
    }

    // Get the tag and type of the changed dhcp option
    amxc_set_parse(&set, dhcp_obj_name_to_type(type));
    tag_str = amxc_var_dyncast(cstring_t, tag);
    amxc_set_add_flag(&set, tag_str);

    update_queries(intf, &set, REASON_DHCP);

exit:
    amxc_set_clean(&set);
    free(tag_str);
    return;
}

void _prefix_changed(UNUSED const char* const sig_name,
                     const amxc_var_t* const data,
                     UNUSED void* const priv) {
    intf_t* intf = get_intf_from_event_data(data);
    when_null(intf, exit);

    update_queries(intf, NULL, REASON_PREFIX);

exit:
    return;
}

void update_mibs(amxd_object_t* object) {
    amxo_parser_apply_mibs(nm_get_parser(), object, nm_intf_matches);
}

const char* query_update_reason_to_str(enum query_update_reason reason) {
    const char* ret = "Invalid";

    switch(reason) {
    case REASON_FLAG:
        ret = "REASON_FLAG";
        break;
    case REASON_LINK:
        ret = "REASON_LINK";
        break;
    case REASON_INTF:
        ret = "REASON_INTF";
        break;
    case REASON_PARAM:
        ret = "REASON_PARAM";
        break;
    case REASON_MIB:
        ret = "REASON_MIB";
        break;
    case REASON_ADDR:
        ret = "REASON_ADDR";
        break;
    case REASON_PATH:
        ret = "REASON_PATH";
        break;
    case REASON_DHCP:
        ret = "REASON_DHCP";
        break;
    case REASON_PREFIX:
        ret = "REASON_PREFIX";
        break;
    default:
        break;
    }

    return ret;
}
