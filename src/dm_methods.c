/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "intf.h"
#include "dm_netmodel.h"
#include "dm_intf.h"

#define ME "netmodel"

static amxd_status_t parse_link_args(amxc_var_t* args, intf_t** ulintf, intf_t** llintf) {

    amxd_status_t status = amxd_status_unknown_error;
    const char* ulintf_name = NULL;
    const char* llintf_name = NULL;
    intf_t* ul = NULL;
    intf_t* ll = NULL;

    ulintf_name = GET_CHAR(args, "ulintf");
    llintf_name = GET_CHAR(args, "llintf");

    ul = intf_find(ulintf_name);
    ll = intf_find(llintf_name);

    if(ul == NULL) {
        SAH_TRACEZ_ERROR(ME, "ulintf not found: %s", ulintf_name);
        status = amxd_status_invalid_function_argument;
        goto exit;
    }

    if(ll == NULL) {
        SAH_TRACEZ_ERROR(ME, "llintf not found: %s", llintf_name);
        status = amxd_status_invalid_function_argument;
        goto exit;
    }

    *ulintf = ul;
    *llintf = ll;
    status = amxd_status_ok;

exit:
    return status;
}

static void get_intfs_by_interface_path(const char* intf_path, amxc_var_t* list) {
    amxc_llist_t nm_list;

    amxc_llist_init(&nm_list);
    amxd_dm_resolve_pathf(nm_get_dm(),
                          &nm_list,
                          "NetModel.Intf.['Device.%s' == InterfacePath]",
                          intf_path);

    amxc_llist_iterate(it, &nm_list) {
        amxd_object_t* obj = amxd_dm_findf(nm_get_dm(), "%s", amxc_string_get(amxc_string_from_llist_it(it), 0));
        amxc_var_add(cstring_t, list, amxd_object_get_name(obj, AMXD_OBJECT_NAMED));
    }
    amxc_llist_clean(&nm_list, amxc_string_list_it_free);
}

static void resolve_path(const char* const path,
                         amxc_var_t* list) {
    amxc_var_t data;
    amxd_path_t p;
    char* fixed_part = NULL;

    // Commented out because of HOP-918
    // amxc_var_t* objects = NULL;
    // amxb_bus_ctx_t* ctx = NULL;

    amxc_var_init(&data);
    amxd_path_init(&p, path);

    // Check if path is a search path
    // If not, there is no need for a lookup
    fixed_part = amxd_path_get_fixed_part(&p, false);
    when_null(fixed_part, exit);
    if(strcmp(fixed_part, path) == 0) {
        get_intfs_by_interface_path(path, list);
        goto exit;
    }

    // Commented out because of HOP-918
    // The amxb_call is causing crashes in ubus
    // ctx = amxb_be_who_has(fixed_part);
    // when_null(ctx, exit);
    // when_failed(amxb_get(ctx, path, 0, &data, 2), exit);
    // objects = GETP_ARG(&data, "0");
    // when_null(objects, exit);

    // amxc_var_for_each(var, objects) {
    //  get_intfs_by_interface_path(amxc_var_key(var), list);
    // }

exit:
    amxc_var_clean(&data);
    free(fixed_part);
    amxd_path_clean(&p);
}

static void on_link_found_cb(intf_t* ulintf, intf_t* llintf, UNUSED bool value, void* userdata) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t params;
    amxc_var_t ret_table;
    amxc_var_t* ret_list = (amxc_var_t*) userdata;

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret_table);
    amxc_var_set_type(&ret_table, AMXC_VAR_ID_HTABLE);

    when_null_trace(ret_list, exit, ERROR, "Bad input parameter ret_list");
    when_null_trace(ulintf, exit, ERROR, "Bad input parameter ulintf");
    when_null_trace(llintf, exit, ERROR, "Bad input parameter llintf");

    status = amxd_object_get_params(intf_object(ulintf), &params, amxd_dm_access_public);
    when_failed_trace(status, exit, ERROR, "Failed to get the parameters of the upperlink interface");
    when_str_empty_trace(GET_CHAR(&params, "InterfaceAlias"), exit, ERROR, "Could not find the param InterfaceAlias of the upperlink");
    when_str_empty_trace(GET_CHAR(&params, "InterfacePath"), exit, ERROR, "Could not find the param InterfacePath of the upperlink");
    amxc_var_set_key(&ret_table, "HigherAlias", GET_ARG(&params, "InterfaceAlias"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(&ret_table, "HigherLayer", GET_ARG(&params, "InterfacePath"), AMXC_VAR_FLAG_COPY);
    amxc_var_clean(&params);

    status = amxd_object_get_params(intf_object(llintf), &params, amxd_dm_access_public);
    when_failed_trace(status, exit, ERROR, "Failed to get the parameters of the lowerlink interface");
    when_str_empty_trace(GET_CHAR(&params, "InterfaceAlias"), exit, ERROR, "Could not find the param InterfaceAlias of the lowerlink");
    when_str_empty_trace(GET_CHAR(&params, "InterfacePath"), exit, ERROR, "Could not find the param InterfacePath of the lowerlink");
    amxc_var_set_key(&ret_table, "LowerAlias", GET_ARG(&params, "InterfaceAlias"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(&ret_table, "LowerLayer", GET_ARG(&params, "InterfacePath"), AMXC_VAR_FLAG_COPY);

    ret_list = amxc_var_add(amxc_htable_t, ret_list, NULL);
    when_null(ret_list, exit);
    amxc_var_move(ret_list, &ret_table);
exit:
    amxc_var_clean(&params);
    amxc_var_clean(&ret_table);
}

amxd_status_t _linkIntfs(UNUSED amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_t* ulintf = NULL;
    intf_t* llintf = NULL;

    status = parse_link_args(args, &ulintf, &llintf);
    if(status == amxd_status_ok) {
        intf_link(ulintf, llintf);
    }

    return status;
}

amxd_status_t _unlinkIntfs(UNUSED amxd_object_t* object,
                           UNUSED amxd_function_t* func,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_t* ulintf = NULL;
    intf_t* llintf = NULL;

    status = parse_link_args(args, &ulintf, &llintf);
    if(status == amxd_status_ok) {
        intf_unlink(ulintf, llintf);
    }

    return status;
}

amxd_status_t _scanMibDir(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* path = NULL;
    int retval = 0;

    path = GET_CHAR(args, "path");
    when_str_empty(path, exit);

    retval = amxo_parser_scan_mib_dir(nm_get_parser(), path);
    if(retval == 0) {
        status = amxd_status_ok;
    }

exit:
    return status;
}

amxd_status_t _resolvePath(UNUSED amxd_object_t* object,
                           UNUSED amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t list;
    amxd_path_t path;
    char* first = NULL;

    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);

    status = amxd_path_init(&path, NULL);
    when_failed(status, exit);

    status = amxd_path_setf(&path, true, "%s", GET_CHAR(args, "path"));
    when_failed(status, exit);

    resolve_path(skip_prefix(&path), &list);

    amxc_var_move(ret, &list);
    status = amxd_status_ok;

exit:
    free(first);
    amxc_var_clean(&list);
    amxd_path_clean(&path);
    return status;
}

amxd_status_t _getLinkInformation(UNUSED amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  UNUSED amxc_var_t* args,
                                  amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(ret, exit, ERROR, "Bad input parameter ret");

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);

    intf_add_linklistener(NULL, NULL, on_link_found_cb, (void*) ret, true);
    intf_del_linklistener(NULL, NULL, on_link_found_cb, (void*) ret, false);

    status = amxd_status_ok;
exit:
    return status;
}

amxd_status_t _getStats(UNUSED amxd_object_t* object,
                        UNUSED amxd_function_t* func,
                        UNUSED amxc_var_t* args,
                        amxc_var_t* ret) {
    netmodel_stats_t* stats = nm_get_stats();
    amxc_var_t* queries = NULL;
    amxc_var_t* var = NULL;
    uint32_t i = 0;
    uint32_t total = 0;
    uint32_t total_time = 0;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "queries opened", stats->queries_opened);
    amxc_var_add_key(uint32_t, ret, "queries closed", stats->queries_closed);
    amxc_var_add_key(uint32_t, ret, "events received", stats->events_received);
    queries = amxc_var_add_key(amxc_htable_t, ret, "queries retriggered", NULL);
    for(i = 0; i < REASON_MAX; i++) {
        var = amxc_var_add_key(amxc_htable_t, queries, query_update_reason_to_str(i), NULL);

        amxc_var_add_key(uint32_t, var, "# of retriggers", stats->queries_retriggered[i]);
        amxc_var_add_key(uint32_t, var, "Time spent in ms", stats->query_time_spent[i]);

        total += stats->queries_retriggered[i];
        total_time += stats->query_time_spent[i];
    }
    var = amxc_var_add_key(amxc_htable_t, queries, "TOTAL", NULL);
    amxc_var_add_key(uint32_t, var, "# of retriggers", total);
    amxc_var_add_key(uint32_t, var, "Time spent in ms", total_time);

    return amxd_status_ok;
}
