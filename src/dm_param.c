/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include "intf.h"
#include "traverse.h"
#include "arg.h"
#include "dm_intf.h"

#define ME "netmodel"

typedef struct _intf_param_ctx {
    amxc_var_t var;
    amxp_expr_t condition;
    char* param;
    const char* path;
    bool single_intf;
} intf_param_ctx_t;

static amxd_status_t execute_intf_param_function(amxd_object_t* object,
                                                 amxc_var_t* args,
                                                 bool (* step)(intf_t* intf, void* userdata),
                                                 intf_param_ctx_t* ctx) {
    amxd_status_t status = amxd_status_unknown_error;
    traverse_mode_t mode;
    intf_t* intf = intf_get_priv(object);
    traverse_tree_t* tree = NULL;
    char* str = NULL;

    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to get interface");
        status = amxd_status_parameter_not_found;
        goto exit;
    }
    when_false(arg_get_traverse(&mode, args, "traverse", &status), exit);
    when_false(arg_get_expr(&ctx->condition, args, "flag", &status), exit);

    when_false(arg_get_string(&ctx->path, args, "name", &status), exit_expr);
    str = strdup(ctx->path);
    ctx->param = strrchr(str, '.');
    if(ctx->param != NULL) {
        *ctx->param++ = '\0';
        ctx->path = str;
    } else {
        ctx->path = NULL;
        ctx->param = str;
    }

    tree = traverse_tree_create(mode, intf);
    if(tree == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to create traverse tree");
        goto exit;
    }

    traverse_tree_walk(tree, step, ctx);
    traverse_tree_destroy(tree);

    status = amxd_status_ok;

exit_expr:
    amxp_expr_clean(&ctx->condition);
exit:
    free(str);
    return status;
}

static bool get_parameter_step(intf_t* intf, void* data) {
    intf_param_ctx_t* ctx = (intf_param_ctx_t*) data;
    amxd_object_t* obj = NULL;
    amxd_param_t* param = NULL;

    when_false(amxp_expr_eval_set(&ctx->condition, intf_flagset(intf), NULL), exit);

    obj = amxd_dm_findf(nm_get_dm(), "NetModel.Intf.%s.%s",
                        intf_name(intf), ctx->path != NULL ? ctx->path : "");
    when_null(obj, exit);
    param = amxd_object_get_param_def(obj, ctx->param);
    when_null(param, exit);

    if(ctx->single_intf) {
        amxd_object_get_param(obj, ctx->param, &ctx->var);
        return true;
    } else {
        amxc_var_t* value = amxc_var_add_new_key(&ctx->var, intf_name(intf));
        amxd_object_get_param(obj, ctx->param, value);
    }

exit:
    return false;
}

static bool set_parameter_step(intf_t* intf, void* data) {
    intf_param_ctx_t* ctx = (intf_param_ctx_t*) data;
    amxd_object_t* obj = NULL;
    amxd_param_t* param = NULL;
    amxd_trans_t trans;
    bool ret = false;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    when_false(amxp_expr_eval_set(&ctx->condition, intf_flagset(intf), NULL), exit);

    obj = amxd_dm_findf(nm_get_dm(), "NetModel.Intf.%s.%s",
                        intf_name(intf), ctx->path != NULL ? ctx->path : "");
    when_null(obj, exit);
    param = amxd_object_get_param_def(obj, ctx->param);
    when_null(param, exit);

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_param(&trans, ctx->param, &ctx->var);
    status = amxd_trans_apply(&trans, nm_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_WARNING(ME, "Failed to set parameter %s.%s",
                           ctx->path != NULL ? ctx->path : "", ctx->param);
    }

    ret = ctx->single_intf;

exit:
    amxd_trans_clean(&trans);
    return ret;
}

amxd_status_t _getParameters(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_param_ctx_t ctx;
    ctx.single_intf = false;

    amxc_var_init(&ctx.var);
    amxc_var_set_type(&ctx.var, AMXC_VAR_ID_HTABLE);

    status = execute_intf_param_function(object, args, get_parameter_step, &ctx);
    when_failed(status, exit);

    amxc_var_move(ret, &ctx.var);

exit:
    amxc_var_clean(&ctx.var);
    return status;
}

amxd_status_t _getFirstParameter(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_param_ctx_t ctx;
    ctx.single_intf = true;

    amxc_var_init(&ctx.var);

    status = execute_intf_param_function(object, args, get_parameter_step, &ctx);
    when_failed(status, exit);

    amxc_var_move(ret, &ctx.var);

exit:
    amxc_var_clean(&ctx.var);
    return status;
}

amxd_status_t _setParameters(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_param_ctx_t ctx;
    ctx.single_intf = false;

    amxc_var_init(&ctx.var);
    when_false(arg_get_var(&ctx.var, args, "value", &status), exit);

    status = execute_intf_param_function(object, args, set_parameter_step, &ctx);

exit:
    amxc_var_clean(&ctx.var);
    return status;
}

amxd_status_t _setFirstParameter(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_param_ctx_t ctx;
    ctx.single_intf = true;

    amxc_var_init(&ctx.var);
    when_false(arg_get_var(&ctx.var, args, "value", &status), exit);

    status = execute_intf_param_function(object, args, set_parameter_step, &ctx);

exit:
    amxc_var_clean(&ctx.var);
    return status;
}
