/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include "intf.h"
#include "traverse.h"
#include "arg.h"
#include "dm_intf.h"

#define ME "netmodel"

typedef struct _intf_prefix_ctx {
    amxc_var_t var;
    amxp_expr_t condition;
    bool single_prefix;
} intf_prefix_ctx_t;

static amxd_status_t execute_intf_prefix_function(amxd_object_t* object,
                                                  amxc_var_t* args,
                                                  bool (* step)(intf_t* intf, void* userdata),
                                                  intf_prefix_ctx_t* ctx) {
    amxd_status_t status = amxd_status_unknown_error;
    traverse_mode_t mode;
    intf_t* intf = intf_get_priv(object);
    traverse_tree_t* tree = NULL;

    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to get interface");
        status = amxd_status_parameter_not_found;
        goto exit;
    }
    when_false(arg_get_traverse(&mode, args, "traverse", &status), exit);
    when_false(arg_get_expr(&ctx->condition, args, "flag", &status), exit);

    tree = traverse_tree_create(mode, intf);
    if(tree == NULL) {
        SAH_TRACEZ_ERROR(ME, "failed to create traverse tree");
        goto exit_expr;
    }

    traverse_tree_walk(tree, step, ctx);
    traverse_tree_destroy(tree);

    status = amxd_status_ok;

exit_expr:
    amxp_expr_clean(&ctx->condition);
exit:
    return status;
}

static int get_prefix_info(amxc_var_t* info,
                           amxd_object_t* prefix_instance,
                           intf_prefix_ctx_t* ctx) {
    amxc_set_t flagset;
    amxc_var_t params;
    amxc_string_t str;
    int ret = -1;
    const char* ra_prefix = "";
    char* status = amxd_object_get_value(cstring_t, prefix_instance, "Status", NULL);
    char* origin = NULL;
    char* type = NULL;

    amxc_var_init(&params);
    amxc_string_init(&str, 0);
    amxc_set_init(&flagset, false);

    when_false(amxd_object_get_value(bool, prefix_instance, "Enable", NULL), exit);
    when_str_empty(status, exit);
    when_false((strcmp(status, "Enabled") == 0), exit);

    origin = amxd_object_get_value(cstring_t, prefix_instance, "Origin", NULL);
    when_null(origin, exit);
    if((strcmp(origin, "Child") == 0) || (strcmp(origin, "AutoConfigured") == 0)) {
        ra_prefix = "RAPrefix";
    }
    type = amxd_object_get_value(cstring_t, prefix_instance, "StaticType", NULL);
    when_null(type, exit);
    amxc_string_setf(&str, "%s %s %s", origin, type, ra_prefix);
    amxc_set_parse(&flagset, amxc_string_get(&str, 0));
    when_false(amxp_expr_eval_set(&ctx->condition, &flagset, NULL), exit);

    amxd_object_get_params(prefix_instance, &params, amxd_dm_access_protected);
    amxc_var_set_key(info, "Origin", GET_ARG(&params, "Origin"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(info, "StaticType", GET_ARG(&params, "StaticType"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(info, "Prefix", GET_ARG(&params, "Prefix"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(info, "PrefixStatus", GET_ARG(&params, "PrefixStatus"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(info, "ValidLifetime", GET_ARG(&params, "ValidLifetime"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(info, "PreferredLifetime", GET_ARG(&params, "PreferredLifetime"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(info, "RelativeValidLifetime", GET_ARG(&params, "RelativeValidLifetime"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(info, "RelativePreferredLifetime", GET_ARG(&params, "RelativePreferredLifetime"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(info, "Autonomous", GET_ARG(&params, "Autonomous"), AMXC_VAR_FLAG_COPY);
    amxc_var_set_key(info, "OnLink", GET_ARG(&params, "OnLink"), AMXC_VAR_FLAG_COPY);
    amxc_var_add_key(uint32_t, info, "Index", prefix_instance->index);

    ret = 0;

exit:
    amxc_set_clean(&flagset);
    amxc_string_clean(&str);
    amxc_var_clean(&params);
    free(status);
    free(origin);
    free(type);
    return ret;
}

static bool get_prefix_step(intf_t* intf, void* data) {
    intf_prefix_ctx_t* ctx = (intf_prefix_ctx_t*) data;
    amxd_object_t* obj = NULL;
    amxd_object_t* prefix_obj = NULL;

    obj = intf_object(intf);
    when_null(obj, exit);

    prefix_obj = amxd_object_get(obj, "IPv6Prefix");
    when_null(prefix_obj, exit);

    amxd_object_iterate(instance, it, prefix_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        amxc_var_t info;

        amxc_var_init(&info);
        amxc_var_set_type(&info, AMXC_VAR_ID_HTABLE);
        if(get_prefix_info(&info, instance, ctx) != 0) {
            amxc_var_clean(&info);
            continue;
        }

        if(ctx->single_prefix) {
            amxc_var_move(&ctx->var, &info);
            amxc_var_clean(&info);
            return true;
        } else {
            amxc_var_t* var = amxc_var_add_new(&ctx->var);
            amxc_var_move(var, &info);
            amxc_var_clean(&info);
        }
    }

exit:
    return false;
}

amxd_status_t _getIPv6Prefixes(amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_prefix_ctx_t ctx;
    ctx.single_prefix = false;

    amxc_var_init(&ctx.var);
    amxc_var_set_type(&ctx.var, AMXC_VAR_ID_LIST);

    status = execute_intf_prefix_function(object, args, get_prefix_step, &ctx);
    when_failed(status, exit);

    amxc_var_move(ret, &ctx.var);

exit:
    amxc_var_clean(&ctx.var);
    return status;
}

amxd_status_t _getFirstIPv6Prefix(amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    intf_prefix_ctx_t ctx;
    ctx.single_prefix = true;

    amxc_var_init(&ctx.var);

    status = execute_intf_prefix_function(object, args, get_prefix_step, &ctx);
    when_failed(status, exit);

    amxc_var_move(ret, &ctx.var);

exit:
    amxc_var_clean(&ctx.var);
    return status;
}
