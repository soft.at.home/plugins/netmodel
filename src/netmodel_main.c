/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "netmodel.h"
#include "dm_netmodel.h"
#include "dm_intf.h"

#define ME "netmodel"

static netmodel_app_t app;

void _count_event(UNUSED const char* const sig_name,
                  UNUSED const amxc_var_t* const data,
                  UNUSED void* const priv) {
    app.stats.events_received++;
}

static void nm_init(amxd_dm_t* dm,
                    amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "started");
    app.dm = dm;
    app.parser = parser;
    memset(&app.stats, 0x0, sizeof(app.stats));

    if(amxo_parser_scan_mib_dir(parser, nm_get_default_mib_dir()) != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to parse mib directory");
    }

    init_intf();
    init_queries();
}

static void nm_exit(UNUSED amxd_dm_t* dm,
                    UNUSED amxo_parser_t* parser) {
    app.dm = NULL;
    app.parser = NULL;

    cleanup_intf();
    cleanup_queries();

    SAH_TRACEZ_INFO(ME, "stopped");
}

amxd_dm_t* PRIVATE nm_get_dm(void) {
    return app.dm;
}

amxo_parser_t* PRIVATE nm_get_parser(void) {
    return app.parser;
}

netmodel_stats_t* nm_get_stats(void) {
    return &app.stats;
}

const char* nm_get_default_mib_dir(void) {
    amxc_var_t* setting = amxo_parser_get_config(nm_get_parser(), "default_mib_dir");
    return amxc_var_constcast(cstring_t, setting);
}

const char* skip_prefix(amxd_path_t* path) {
    char* first = NULL;

    first = amxd_path_get_first(path, false);
    if((first != NULL) && (strcmp("Device.", first) == 0)) {
        free(first);
        first = amxd_path_get_first(path, true);
    }

    free(first);

    return amxd_path_get(path, AMXD_OBJECT_TERMINATE);
}

int _nm_main(int reason,
             amxd_dm_t* dm,
             amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "entry point %s, reason: %d", __func__, reason);
    switch(reason) {
    case 0:     // START
        nm_init(dm, parser);
        break;
    case 1:     // STOP
        nm_exit(dm, parser);
        break;
    default:
        break;
    }

    return 0;
}

