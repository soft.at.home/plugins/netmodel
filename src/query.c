/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "query.h"

#define ME "netmodel"

typedef struct _listener_list {
    amxc_llist_it_t it;
    query_listener_t* listener;
    void* userdata;
} listener_list_t;

static amxc_llist_t classes = {NULL, NULL};
static amxc_llist_t listeners = {NULL, NULL};

static unsigned int query_count = 0;

static void query_notify_opened(query_t* q) {
    amxc_llist_iterate(it, &listeners) {
        listener_list_t* ll = amxc_container_of(it, listener_list_t, it);
        if(ll->listener->opened != NULL) {
            ll->listener->opened(q, ll->userdata);
        }
    }
}

static void query_notify_closed(query_t* q) {
    amxc_llist_iterate(it, &listeners) {
        listener_list_t* ll = amxc_container_of(it, listener_list_t, it);
        if(ll->listener->closed != NULL) {
            ll->listener->closed(q, ll->userdata);
        }
    }
}

static void query_notify_altered(query_t* q) {
    amxc_llist_iterate(it, &listeners) {
        listener_list_t* ll = amxc_container_of(it, listener_list_t, it);
        if(ll->listener->altered != NULL) {
            ll->listener->altered(q, ll->userdata);
        }
    }
}

static void query_notify_triggered(query_t* q) {
    amxc_llist_iterate(it, &listeners) {
        listener_list_t* ll = amxc_container_of(it, listener_list_t, it);
        if(ll->listener->triggered != NULL) {
            ll->listener->triggered(q, ll->userdata);
        }
    }
}

static query_t* query_new(query_class_t* cl,
                          amxd_object_t* obj,
                          amxc_var_t* args) {
    query_t* q = NULL;
    intf_t* intf = NULL;

    when_null_trace(cl, exit, ERROR, "Query class is NULL");
    when_null_trace(obj, exit, ERROR, "Interface object is NULL");

    intf = (intf_t*) obj->priv;
    when_null_trace(intf, exit, ERROR, "Interface object private data is NULL");

    q = (query_t*) calloc(1, sizeof(query_t));
    when_null_trace(q, exit, ERROR, "Failed to allocate query structure");

    q->intf = obj;
    q->query_class = cl;
    q->id = ++query_count;
    amxc_var_init(&q->args);
    amxc_var_copy(&q->args, args);
    amxc_var_init(&q->result);

    amxc_set_new(&q->subscribers, true);
    when_null_trace(q->subscribers, error, ERROR, "Failed to create flagset");

    amxc_llist_append(&cl->queries, &q->it);
    amxc_llist_append(intf_queries(intf), &q->intf_it);

    return q;

error:
    amxc_var_clean(&q->args);
    amxc_var_clean(&q->result);
    free(q);
    q = NULL;
exit:
    return q;
}

query_class_t* query_class_register(const char* func) {
    query_class_t* cl = NULL;
    amxd_object_t* obj = NULL;

    if(func == NULL) {
        SAH_TRACEZ_ERROR(ME, "Function is NULL");
        goto exit;
    }

    cl = (query_class_t*) calloc(1, sizeof(query_class_t));
    if(cl == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to allocate query class");
        goto exit;
    }

    amxc_llist_append(&classes, &cl->it);

    obj = amxd_dm_findf(nm_get_dm(), "NetModel.Intf.");
    cl->function = amxd_object_get_function(obj, func);
    if(cl->function == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not find function %s for NetModel.Intf", func);
        amxc_llist_it_take(&cl->it);
        free(cl);
        cl = NULL;
    }

exit:
    return cl;
}

void query_class_unregister(query_class_t* cl) {
    when_null(cl, exit);

    amxc_llist_for_each(it, &cl->queries) {
        query_t* q = amxc_container_of(it, query_t, it);
        query_destroy(q);
    }

    amxc_llist_it_take(&cl->it);
    free(cl);

exit:
    return;
}

query_class_t* query_class_find(const char* func) {
    query_class_t* cl = NULL;

    if(func == NULL) {
        SAH_TRACEZ_ERROR(ME, "func is NULL");
        goto exit;
    }

    for(cl = query_class_first(); cl; cl = query_class_next(cl)) {
        if(strcmp(func, cl->function->name) == 0) {
            break;
        }
    }

exit:
    return cl;
}

amxd_function_t* query_class_function(const query_class_t* cl) {
    if(cl == NULL) {
        SAH_TRACEZ_ERROR(ME, "cl is NULL");
        return NULL;
    }
    return cl->function;
}

query_class_t* query_class_first(void) {
    return amxc_container_of(amxc_llist_get_first(&classes), query_class_t, it);
}

query_class_t* query_class_next(query_class_t* cl) {
    if(cl == NULL) {
        SAH_TRACEZ_ERROR(ME, "cl is NULL");
        return NULL;
    }
    return amxc_container_of(amxc_llist_it_get_next(&cl->it), query_class_t, it);
}

query_t* query_open(const char* subscriber,
                    query_class_t* cl,
                    amxd_object_t* intf,
                    amxc_var_t* args) {
    query_t* q = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    int ret = -1;
    netmodel_stats_t* stats = nm_get_stats();

    for(q = query_first(cl); q; q = query_next(q)) {
        if((q->intf == intf) &&
           ( q->query_class == cl) &&
           ( amxc_var_compare(&q->args, args, &ret) == 0) &&
           ( ret == 0)) {
            break;
        }
    }

    if(q == NULL) {
        q = query_new(cl, intf, args);
        when_null(q, exit);
        query_notify_opened(q);
        stats->queries_opened++;
        status = amxd_object_invoke_function(intf, cl->function->name, args, &q->result);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to execute query function %s, return value: %d", q->query_class->function->name, status);
            goto exit;
        }
        query_notify_triggered(q);
    } else {
        // Amx returns unused arguments in the return variant
        // Normally, the query function arguments are taken out when invoking the query function
        // In this case, the query already exists and the function is not executed again
        // Clean up the args here so the return variant is consistent in all cases
        amxc_var_clean(args);
    }

    amxc_set_add_flag(q->subscribers, subscriber);
    query_notify_altered(q);

exit:
    return q;
}

void query_destroy(query_t* q) {
    netmodel_stats_t* stats = nm_get_stats();
    when_null(q, exit);

    amxc_var_clean(&q->result);
    amxc_var_clean(&q->args);
    amxc_set_delete(&q->subscribers);
    amxc_llist_it_take(&q->it);
    amxc_llist_it_take(&q->intf_it);
    free(q);
    stats->queries_closed++;

exit:
    return;
}

unsigned int query_close(const char* subscriber,
                         query_class_t* cl,
                         amxd_object_t* intf,
                         amxc_var_t* args) {
    query_t* q = NULL;
    unsigned int id = 0;
    int ret = -1;

    for(q = query_first(cl); q; q = query_next(q)) {
        if((q->intf == intf) &&
           ( q->query_class == cl) &&
           ( amxc_var_compare(&q->args, args, &ret) == 0) &&
           ( ret == 0)) {
            break;
        }
    }

    if(q == NULL) {
        SAH_TRACEZ_ERROR(ME, "Query not found");
        goto exit;
    }

    id = q->id;
    amxc_set_remove_flag(q->subscribers, subscriber);
    query_notify_altered(q);

    if(amxc_set_get_count(q->subscribers, NULL) == 0) {
        query_notify_closed(q);
    }

exit:
    return id;
}

query_t* query_first(query_class_t* cl) {
    if(cl == NULL) {
        SAH_TRACEZ_ERROR(ME, "cl is NULL");
        return NULL;
    }
    return amxc_container_of(amxc_llist_get_first(&cl->queries), query_t, it);
}

query_t* query_next(query_t* q) {
    if(q == NULL) {
        SAH_TRACEZ_ERROR(ME, "q is NULL");
        return NULL;
    }
    return amxc_container_of(amxc_llist_it_get_next(&q->it), query_t, it);
}

unsigned int query_id(query_t* q) {
    if(q == NULL) {
        SAH_TRACEZ_ERROR(ME, "q is NULL");
        return 0;
    }
    return q->id;
}

query_t* query_find(unsigned int id) {
    query_t* ret = NULL;

    amxc_llist_iterate(it, &classes) {
        query_class_t* cl = amxc_container_of(it, query_class_t, it);
        amxc_llist_iterate(q_it, &cl->queries) {
            query_t* q = amxc_container_of(q_it, query_t, it);
            if(q->id == id) {
                ret = q;
                goto exit;
            }
        }
    }

exit:
    return ret;
}

char* query_describe(query_t* q) {
    amxc_string_t description;
    char* obj_path = NULL;
    const amxc_htable_t* args_ht = NULL;
    amxc_array_t* keys = NULL;
    size_t size = 0;

    if(q == NULL) {
        SAH_TRACEZ_ERROR(ME, "q is NULL");
        return NULL;
    }

    amxc_string_init(&description, 0);

    obj_path = amxd_object_get_path(q->intf, AMXD_OBJECT_NAMED);
    amxc_string_append(&description, obj_path, strlen(obj_path));
    free(obj_path);

    amxc_string_appendf(&description, ".%s", q->query_class->function->name);
    args_ht = amxc_var_constcast(amxc_htable_t, &q->args);

    keys = amxc_htable_get_sorted_keys(args_ht);
    size = amxc_array_size(keys);
    for(unsigned int i = 0; i < size; i++) {
        const char* key = (const char*) amxc_array_get_data_at(keys, i);
        char* value = amxc_var_dyncast(cstring_t, GET_ARG(&q->args, key));

        amxc_string_append(&description, i == 0 ? "(" : " ", 1);
        amxc_string_appendf(&description, "%s=\"%s\"", key, value);
        amxc_string_append(&description, i == (size - 1) ? ")" : ",", 1);

        free(value);
    }
    amxc_array_delete(&keys, NULL);

    return (char*) amxc_string_get(&description, 0);
}

amxc_set_t* query_subscribers(query_t* q) {
    if(q == NULL) {
        SAH_TRACEZ_ERROR(ME, "q is NULL");
        return NULL;
    }
    return q->subscribers;
}

const amxc_var_t* query_result(query_t* q) {
    if(q == NULL) {
        SAH_TRACEZ_ERROR(ME, "q is NULL");
        return NULL;
    }
    return &q->result;
}

const amxc_var_t* query_args(query_t* q) {
    if(q == NULL) {
        SAH_TRACEZ_ERROR(ME, "q is NULL");
        return NULL;
    }
    return &q->args;
}

amxd_object_t* query_intf(query_t* q) {
    if(q == NULL) {
        SAH_TRACEZ_ERROR(ME, "q is NULL");
        return NULL;
    }
    return q->intf;
}

query_class_t* query_class(query_t* q) {
    if(q == NULL) {
        SAH_TRACEZ_ERROR(ME, "q is NULL");
        return NULL;
    }
    return q->query_class;
}

void query_invalidate(query_t* q) {
    amxc_var_t ret;
    amxc_var_t args;
    amxd_status_t status = amxd_status_unknown_error;
    int result = -1;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    SAH_TRACEZ_INFO(ME, "Retriggering query %u", q->id);

    // Invoking the function takes the arguments from the args variable, make a copy
    amxc_var_copy(&args, &q->args);
    status = amxd_object_invoke_function(q->intf, q->query_class->function->name, &args, &ret);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to execute query function %s, return value: %d", q->query_class->function->name, status);
        goto exit;
    }

    when_true(amxc_var_is_null(&ret) && amxc_var_is_null(&q->result), exit);
    if((amxc_var_compare(&ret, &q->result, &result) != 0) ||
       (result != 0)) {
        amxc_var_copy(&q->result, &ret);
        query_notify_triggered(q);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

void query_add_listener(query_listener_t* l, void* userdata) {
    listener_list_t* ll = NULL;

    if(l == NULL) {
        SAH_TRACEZ_ERROR(ME, "l is NULL");
        return;
    }

    ll = (listener_list_t*) calloc(1, sizeof(listener_list_t));
    if(ll == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to allocate listener");
        return;
    }
    ll->listener = l;
    ll->userdata = userdata;
    amxc_llist_append(&listeners, &ll->it);
}

void query_del_listener(query_listener_t* l, void* userdata) {
    amxc_llist_it_t* it = NULL;
    listener_list_t* ll = NULL;
    if(l == NULL) {
        SAH_TRACEZ_ERROR(ME, "l is NULL");
        return;
    }

    for(it = amxc_llist_get_first(&listeners); it != NULL; it = amxc_llist_it_get_next(it)) {
        ll = amxc_container_of(it, listener_list_t, it);
        if((l == ll->listener) && (userdata == ll->userdata)) {
            break;
        }
    }
    if(it == NULL) {
        SAH_TRACEZ_ERROR(ME, "listener not found");
        return;
    }

    amxc_llist_it_take(it);
    free(ll);
}

