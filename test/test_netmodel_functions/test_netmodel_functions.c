/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <amxo/amxo.h>

#include "intf.h"
#include "query.h"
#include "dm_intf.h"
#include "dm_query.h"

#include "test_netmodel_functions.h"
#include "test_utility.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* mib_dir = "./";
static const char* odl_defs = "../../odl/netmodel_definition.odl";
static const char* dhcp_opts_odl = "mock_dhcp_populate.odl";
static const char* test_config_odl = "../common/mock_config.odl";

static const int nr_of_intfs = 3;

int test_functions_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_trans_t trans;
    int i = 0;
    amxc_string_t str;

    amxc_string_init(&str, 0);
    assert_int_equal(amxd_trans_init(&trans), 0);

    test_init_dm(&dm, &parser);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, test_config_odl, root_obj), 0);
    assert_int_equal(amxo_parser_scan_mib_dir(&parser, mib_dir), 0);

    handle_events();

    assert_int_equal(_nm_main(0, &dm, &parser), 0);

    amxd_trans_select_pathf(&trans, "NetModel.Intf.");

    for(i = 0; i < nr_of_intfs; i++) {
        amxc_string_setf(&str, "intf_%d", i);
        amxd_trans_add_inst(&trans, 0, amxc_string_get(&str, 0));
        amxd_trans_set_value(cstring_t, &trans, "Name", amxc_string_get(&str, 0));
        amxd_trans_set_value(cstring_t, &trans, "InterfaceAlias", amxc_string_get(&str, 0));
        amxc_string_setf(&str, "Device.NetModel.Intf.%d.", i + 1);
        amxd_trans_set_value(cstring_t, &trans, "InterfacePath", amxc_string_get(&str, 0));
        amxd_trans_select_pathf(&trans, ".^");
    }

    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    intf_link(intf_find("intf_0"), intf_find("intf_1"));
    intf_link(intf_find("intf_1"), intf_find("intf_2"));
    handle_events();

    // Create dummy/fake bus connections
    test_register_dummy_be();
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    assert_int_equal(amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx), 0);
    // Register data model
    assert_int_equal(amxb_register(bus_ctx, &dm), 0);

    amxd_trans_clean(&trans);
    amxc_string_clean(&str);
    return 0;
}

int test_functions_teardown(UNUSED void** state) {
    assert_int_equal(_nm_main(1, &dm, &parser), 0);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    test_unregister_dummy_be();

    return 0;
}

static void intf_set_status(const char* intf, bool status) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.%s", intf);
    amxd_trans_t trans;

    assert_non_null(intf_obj);
    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_object(&trans, intf_obj);
    amxd_trans_set_value(bool, &trans, "Status", status);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();
    amxd_trans_clean(&trans);
}

static amxd_status_t invoke_dm_func_f_t(amxd_object_t* obj,
                                        const char* func,
                                        const char* flag,
                                        const char* traverse,
                                        amxc_var_t* ret) {
    amxc_var_t args;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", flag);
    amxc_var_add_key(cstring_t, &args, "traverse", traverse);
    status = invoke_dm_func(obj, func, &args, ret);
    amxc_var_clean(&args);

    return status;
}


static amxd_status_t invoke_dm_func_t(amxd_object_t* obj,
                                      const char* func,
                                      const char* target,
                                      amxc_var_t* ret) {
    amxc_var_t args;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "target", target);
    status = invoke_dm_func(obj, func, &args, ret);
    amxc_var_clean(&args);

    return status;
}

static amxd_status_t invoke_dm_func_p(amxd_object_t* obj,
                                      const char* func,
                                      const char* path,
                                      amxc_var_t* ret) {
    amxc_var_t args;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "path", path);
    status = invoke_dm_func(obj, func, &args, ret);
    amxc_var_clean(&args);

    return status;
}

static amxd_status_t invoke_dm_func_n_f_t(amxd_object_t* obj,
                                          const char* func,
                                          const char* name,
                                          const char* flag,
                                          const char* traverse,
                                          amxc_var_t* ret) {
    amxc_var_t args;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    amxc_var_add_key(cstring_t, &args, "flag", flag);
    amxc_var_add_key(cstring_t, &args, "traverse", traverse);
    status = invoke_dm_func(obj, func, &args, ret);
    amxc_var_clean(&args);

    return status;
}

static amxd_status_t invoke_dm_func_n_v_f_t(amxd_object_t* obj,
                                            const char* func,
                                            const char* name,
                                            amxc_var_t* value,
                                            const char* flag,
                                            const char* traverse,
                                            amxc_var_t* ret) {
    amxc_var_t args;
    amxc_var_t* val = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&args);
    amxc_var_copy(val, value);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    amxc_var_add_key(cstring_t, &args, "flag", flag);
    amxc_var_add_key(cstring_t, &args, "traverse", traverse);
    val = amxc_var_add_new_key(&args, "value");
    amxc_var_copy(val, value);
    status = invoke_dm_func(obj, func, &args, ret);
    amxc_var_clean(&args);

    return status;
}

static amxd_status_t invoke_dm_func_t_t_t(amxd_object_t* obj,
                                          const char* func,
                                          const char* type,
                                          uint16_t tag,
                                          const char* traverse,
                                          amxc_var_t* ret) {
    amxc_var_t args;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(uint16_t, &args, "tag", tag);
    amxc_var_add_key(cstring_t, &args, "traverse", traverse);
    status = invoke_dm_func(obj, func, &args, ret);
    amxc_var_clean(&args);

    return status;
}


void test_func_isUp(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    // Intf_0 should be down
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", "", "this", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));

    // Set intf_0 up, recheck
    intf_set_status("intf_0", true);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", "", "this", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));

    // Checking the entire hierarchy should return false, intfs 1 and 2 are down
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", "", "down", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));

    // Set intfs 1 and 2 up
    intf_set_status("intf_1", true);
    intf_set_status("intf_2", true);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", "", "down", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));

    // Not all params are mandatory
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", NULL, NULL, &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
}

void test_func_isLinkedTo(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    // Intfs should be linked: intf_0 <-> intf_1 <-> intf_2
    assert_int_equal(invoke_dm_func_t(intf_obj, "isLinkedTo", "intf_1", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
    assert_int_equal(invoke_dm_func_t(intf_obj, "isLinkedTo", "intf_2", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));

    // Unlink intf 1 and 2
    intf_unlink(intf_find("intf_1"), intf_find("intf_2"));
    assert_int_equal(invoke_dm_func_t(intf_obj, "isLinkedTo", "intf_1", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));
    assert_int_equal(invoke_dm_func_t(intf_obj, "isLinkedTo", "intf_2", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));

    // Non existent intf should return false
    assert_int_equal(invoke_dm_func_t(intf_obj, "isLinkedTo", "nope", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));

    // Missing param
    assert_int_not_equal(invoke_dm_func_t(intf_obj, "isLinkedTo", NULL, &ret), 0);
}

static bool check_llist_intfs(unsigned int size, const amxc_llist_t* list) {
    char name[64] = {0};
    const char* str = NULL;
    amxc_llist_it_t* it = NULL;

    for(unsigned int i = 0; i < size; i++) {
        snprintf(name, sizeof(name), "intf_%d", i);
        it = amxc_llist_get_at(list, i);
        if(it == NULL) {
            return false;
        }
        str = amxc_var_constcast(cstring_t, amxc_llist_it_get_data(it, amxc_var_t, lit));
        if((str == NULL) || (strcmp(str, name) != 0)) {
            return false;
        }
    }

    return true;
}

void test_func_getIntfs(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    // GetIntfs should return a list with intfs 0, 1 and 2
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getIntfs", "", "down", &ret), 0);
    assert_true(check_llist_intfs(3, amxc_var_constcast(amxc_llist_t, &ret)));

    // GetIntfs should return a list with intfs 0 and 1
    intf_unlink(intf_find("intf_1"), intf_find("intf_2"));
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getIntfs", "", "down", &ret), 0);
    assert_true(check_llist_intfs(2, amxc_var_constcast(amxc_llist_t, &ret)));

    // GetIntfs should return a list with intf 0
    intf_unlink(intf_find("intf_0"), intf_find("intf_1"));
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getIntfs", "", "down", &ret), 0);
    assert_true(check_llist_intfs(1, amxc_var_constcast(amxc_llist_t, &ret)));

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getIntfs", "", "up exclusive", &ret), 0);
    assert_false(check_llist_intfs(3, amxc_var_constcast(amxc_llist_t, &ret)));

    amxc_var_clean(&ret);
}

void test_func_luckyIntf(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "down", &ret), 0);
    assert_int_equal(strcmp("intf_0", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "this", &ret), 0);
    assert_int_equal(strcmp("intf_0", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "down exclusive", &ret), 0);
    assert_int_equal(strcmp("intf_1", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "one level down", &ret), 0);
    assert_int_equal(strcmp("intf_1", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "one level up", &ret), 0);
    assert_int_equal(strcmp("", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "up exclusive", &ret), 0);
    assert_int_equal(strcmp("", amxc_var_constcast(cstring_t, &ret)), 0);

    intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_1");
    assert_non_null(intf_obj);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "down", &ret), 0);
    assert_int_equal(strcmp("intf_1", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "this", &ret), 0);
    assert_int_equal(strcmp("intf_1", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "down exclusive", &ret), 0);
    assert_int_equal(strcmp("intf_2", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "one level down", &ret), 0);
    assert_int_equal(strcmp("intf_2", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "one level up", &ret), 0);
    assert_int_equal(strcmp("intf_0", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "up exclusive", &ret), 0);
    assert_int_equal(strcmp("intf_0", amxc_var_constcast(cstring_t, &ret)), 0);

    intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_2");
    assert_non_null(intf_obj);

    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "down", &ret), 0);
    assert_int_equal(strcmp("intf_2", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "this", &ret), 0);
    assert_int_equal(strcmp("intf_2", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "down exclusive", &ret), 0);
    assert_int_equal(strcmp("", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "one level down", &ret), 0);
    assert_int_equal(strcmp("", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "one level up", &ret), 0);
    assert_int_equal(strcmp("intf_1", amxc_var_constcast(cstring_t, &ret)), 0);
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "luckyIntf", "", "up exclusive", &ret), 0);
    assert_int_equal(strcmp("intf_1", amxc_var_constcast(cstring_t, &ret)), 0);

    amxc_var_clean(&ret);
}

static bool check_ht_intf_names(unsigned int size, const amxc_htable_t* ht) {
    char name[64] = {0};
    const char* str = NULL;
    amxc_htable_it_t* it = NULL;

    for(unsigned int i = 0; i < size; i++) {
        snprintf(name, sizeof(name), "intf_%d", i);
        it = amxc_htable_get(ht, name);
        if(it == NULL) {
            return false;
        }
        str = amxc_var_constcast(cstring_t, amxc_container_of(it, amxc_var_t, hit));
        if((str == NULL) || (strcmp(str, name) != 0)) {
            return false;
        }
    }

    return true;
}

void test_func_getParameters(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    assert_int_equal(invoke_dm_func_n_f_t(intf_obj, "getParameters", "Name", "", "down", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE);
    assert_true(check_ht_intf_names(3, amxc_var_constcast(amxc_htable_t, &ret)));

    assert_int_equal(invoke_dm_func_n_f_t(intf_obj, "getParameters", "Name", "", "this", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE);
    assert_true(check_ht_intf_names(1, amxc_var_constcast(amxc_htable_t, &ret)));

    assert_int_equal(invoke_dm_func_n_f_t(intf_obj, "getParameters", "Name", "", "up exclusive", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE);
    assert_false(check_ht_intf_names(3, amxc_var_constcast(amxc_htable_t, &ret)));

    assert_int_equal(invoke_dm_func_n_f_t(intf_obj, "getParameters", "Status", "", "up exclusive", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_HTABLE);
    amxc_var_for_each(var, &ret) {
        assert_true(amxc_var_type_of(var) == AMXC_VAR_ID_BOOL);
        assert_false(amxc_var_constcast(bool, var));
    }

    assert_int_not_equal(invoke_dm_func_n_f_t(intf_obj, "getParameters", NULL, "", "down", &ret), 0);

    amxc_var_clean(&ret);
}

void test_func_getFirstParameter(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    assert_int_equal(invoke_dm_func_n_f_t(intf_obj, "getFirstParameter", "Name", "", "down", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_CSTRING);
    assert_int_equal(strcmp("intf_0", amxc_var_constcast(cstring_t, &ret)), 0);

    assert_int_equal(invoke_dm_func_n_f_t(intf_obj, "getFirstParameter", "Status", "", "down", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_BOOL);
    assert_false(amxc_var_constcast(bool, &ret));

    intf_set_status("intf_1", true);
    assert_int_equal(invoke_dm_func_n_f_t(intf_obj, "getFirstParameter", "Status", "", "down exclusive", &ret), 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_BOOL);
    assert_true(amxc_var_constcast(bool, &ret));

    assert_int_not_equal(invoke_dm_func_n_f_t(intf_obj, "getFirstParameter", NULL, "", "down", &ret), 0);

    amxc_var_clean(&ret);
}

void test_func_setParameters(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    amxc_var_t var;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_var_init(&var);

    // Status should be false initially
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", "", "down", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));

    // Set status to true for all intfs
    amxc_var_set(bool, &var, true);
    assert_int_equal(invoke_dm_func_n_v_f_t(intf_obj, "setParameters", "Status", &var, "", "down", &ret), 0);

    // Status should be true for all intfs
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", "", "down", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));

    amxc_var_clean(&ret);
    amxc_var_clean(&var);
}

void test_func_setFirstParameter(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;
    amxc_var_t var;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);
    amxc_var_init(&var);

    // Status should be false initially
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", "", "down", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));

    // Set status to true for intf 0
    amxc_var_set(bool, &var, true);
    assert_int_equal(invoke_dm_func_n_v_f_t(intf_obj, "setFirstParameter", "Status", &var, "", "down", &ret), 0);

    // Intf 0 should be up
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", "", "this", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));

    // When considering intfs 1 and 2 as well, should still be considered down
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", "", "down", &ret), 0);
    assert_false(amxc_var_constcast(bool, &ret));

    // Set intf 1 up
    assert_int_equal(invoke_dm_func_n_v_f_t(intf_obj, "setFirstParameter", "Status", &var, "", "down exclusive", &ret), 0);

    intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_2");
    assert_non_null(intf_obj);

    // Set intf 2 up
    assert_int_equal(invoke_dm_func_n_v_f_t(intf_obj, "setFirstParameter", "Status", &var, "", "this", &ret), 0);

    // All intfs should be up
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "isUp", "", "down", &ret), 0);
    assert_true(amxc_var_constcast(bool, &ret));

    amxc_var_clean(&ret);
    amxc_var_clean(&var);
}

void test_func_resolvePath(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    // Commented out because of HOP-918
    // assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "NetModel.Intf.*.", &ret), 0);
    // assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 3);

    // assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "Device.NetModel.Intf.*.", &ret), 0);
    // assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 3);

    assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "Does.Not.Exist", &ret), 0);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 0);

    assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "NetModel.Intf.1.", &ret), 0);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 1);

    assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "NetModel.Intf.", &ret), 0);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 0);

    assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "Device.NetModel.Intf.", &ret), 0);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 0);

    assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "Device.NetModel.Intf.1.", &ret), 0);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 1);

    assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "Device.NetModel.Intf.1", &ret), 0);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 1);

    assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "Device.NetModel.Intf.2.", &ret), 0);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 1);

    assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "NetModel.Intf.2", &ret), 0);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 1);

    assert_int_equal(invoke_dm_func_p(intf_obj, "resolvePath", "", &ret), 0);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &ret))), 0);

    assert_int_not_equal(invoke_dm_func_p(intf_obj, "resolvePath", NULL, &ret), 0);

    amxc_var_clean(&ret);
}

void test_func_getDHCPOption(UNUSED void** state) {
    amxd_object_t* intf_obj = NULL;
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    amxc_var_t ret;

    assert_non_null(root_obj);
    amxc_var_init(&ret);

    // Load Intf containing dhcp options
    assert_int_equal(amxo_parser_parse_file(&parser, dhcp_opts_odl, root_obj), 0);
    handle_events();
    intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.dhcp.");
    assert_non_null(intf_obj);

    // Check input args
    assert_int_not_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", NULL, 1, "this", &ret), 0);
    assert_int_not_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "invalid", 1, "this", &ret), 0);
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req", 1, "this", &ret), 0);
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req6", 1, "this", &ret), 0);
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "sent", 1, "this", &ret), 0);
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "sent6", 1, "this", &ret), 0);
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "ra", 1, "this", &ret), 0);

    // V4 options present
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req", 1, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req", 3, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req", 6, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req", 15, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req", 51, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));

    // V4 options not present
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req", 2, "this", &ret), 0);
    assert_true(amxc_var_is_null(&ret));

    // V6 options present
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req6", 1, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req6", 2, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req6", 3, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req6", 7, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req6", 24, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req6", 25, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req6", 31, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));

    // V6 options not present
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req6", 5, "this", &ret), 0);
    assert_true(amxc_var_is_null(&ret));

    // V6 options not present multi
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "req6", 4, "this", &ret), 0);
    assert_int_equal(amxc_var_type_of(&ret), AMXC_VAR_ID_LIST);

    // RA option present
    assert_int_equal(invoke_dm_func_t_t_t(intf_obj, "getDHCPOption", "ra", 3, "this", &ret), 0);
    assert_false(amxc_var_is_null(&ret));

    amxc_var_clean(&ret);
}

void test_func_getMibs(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    amxc_var_t ret;

    assert_non_null(intf_obj);
    amxc_var_init(&ret);

    // get mibs, should be empty
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getMibs", "", "this", &ret), 0);
    assert_int_equal(amxc_var_type_of(&ret), AMXC_VAR_ID_HTABLE);
    assert_string_equal(GET_CHAR(&ret, "intf_0"), "");

    // set dhcpv4 flag
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "setFlag", "dhcpv4", "this", &ret), 0);

    // get mibs, should contain dhcpv4
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getMibs", "", "this", &ret), 0);
    assert_int_equal(amxc_var_type_of(&ret), AMXC_VAR_ID_HTABLE);
    assert_string_equal(GET_CHAR(&ret, "intf_0"), "dhcpv4");

    // set dhcpv6 flag
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "setFlag", "dhcpv6", "this", &ret), 0);

    // get mibs, should contain both
    assert_int_equal(invoke_dm_func_f_t(intf_obj, "getMibs", "", "this", &ret), 0);
    assert_int_equal(amxc_var_type_of(&ret), AMXC_VAR_ID_HTABLE);
    assert_string_equal(GET_CHAR(&ret, "intf_0"), "dhcpv4 dhcpv6");

    amxc_var_clean(&ret);
}

void test_func_getLinkInformation(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(&dm, "NetModel.");
    amxd_object_t* intf_obj = NULL;
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    assert_non_null(intf_obj);
    intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_1");
    assert_non_null(intf_obj);
    intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_2");
    assert_non_null(intf_obj);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(template, "getLinkInformation", &args, &ret), 0);

    assert_int_equal(strcmp(GETP_CHAR(&ret, "0.HigherAlias"), "intf_0"), 0);
    assert_int_equal(strcmp(GETP_CHAR(&ret, "0.HigherLayer"), "Device.NetModel.Intf.1."), 0);
    assert_int_equal(strcmp(GETP_CHAR(&ret, "0.LowerAlias"), "intf_1"), 0);
    assert_int_equal(strcmp(GETP_CHAR(&ret, "0.LowerLayer"), "Device.NetModel.Intf.2."), 0);

    assert_int_equal(strcmp(GETP_CHAR(&ret, "1.HigherAlias"), "intf_1"), 0);
    assert_int_equal(strcmp(GETP_CHAR(&ret, "1.HigherLayer"), "Device.NetModel.Intf.2."), 0);
    assert_int_equal(strcmp(GETP_CHAR(&ret, "1.LowerAlias"), "intf_2"), 0);
    assert_int_equal(strcmp(GETP_CHAR(&ret, "1.LowerLayer"), "Device.NetModel.Intf.3."), 0);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}
