/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <amxo/amxo.h>

#include "intf.h"
#include "dm_intf.h"

#include "test_netmodel_intf.h"
#include "test_utility.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "../../odl/netmodel_definition.odl";

static const int nr_of_intfs = 2;

int test_intf_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_trans_t trans;
    int i = 0;
    char name[64];

    assert_int_equal(amxd_trans_init(&trans), 0);

    test_init_dm(&dm, &parser);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    handle_events();

    assert_int_equal(_nm_main(0, &dm, &parser), 0);

    amxd_trans_select_pathf(&trans, "NetModel.Intf.");

    for(i = 0; i < nr_of_intfs; i++) {
        snprintf(name, sizeof(name), "intf_%d", i);
        amxd_trans_add_inst(&trans, 0, name);
        amxd_trans_select_pathf(&trans, ".^");
    }

    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    handle_events();

    amxd_trans_clean(&trans);
    return 0;
}

int test_intf_teardown(UNUSED void** state) {
    assert_int_equal(_nm_main(1, &dm, &parser), 0);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

static void execute_link_function(const char* ulintf, const char* llintf, const char* func) {
    amxd_object_t* template = amxd_dm_findf(&dm, "NetModel.");
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "ulintf", ulintf);
    amxc_var_add_key(cstring_t, &args, "llintf", llintf);
    assert_int_equal(amxd_object_invoke_function(template, func, &args, &ret), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void link_intfs(const char* ulintf, const char* llintf) {
    execute_link_function(ulintf, llintf, "linkIntfs");
}

static void unlink_intfs(const char* ulintf, const char* llintf) {
    execute_link_function(ulintf, llintf, "unlinkIntfs");
}

// Verify the linked/unlinked conditions, will only return true when all conditions are met
static bool check_link(const char* ulintf, const char* llintf, bool should_be_linked) {
    bool ret = false;
    bool found = false;
    amxd_object_t* obj = NULL;
    intf_link_t* link = NULL;
    intf_t* ul = intf_find(ulintf);
    intf_t* ll = intf_find(llintf);

    if(should_be_linked) {
        when_null(ul, out);
        when_null(ll, out);
    } else {
        when_null(ul, out_unlinked);
        when_null(ll, out_unlinked);
    }

    // Check datamodel ulintf.LLIntf
    obj = amxd_dm_findf(&dm, "NetModel.Intf.%s.LLIntf.%s", ulintf, llintf);
    if(should_be_linked) {
        when_null(obj, out);
    } else {
        when_not_null(obj, out);
    }

    // Check datamodel llintf.ULIntf
    obj = amxd_dm_findf(&dm, "NetModel.Intf.%s.ULIntf.%s", llintf, ulintf);
    if(should_be_linked) {
        when_null(obj, out);
    } else {
        when_not_null(obj, out);
    }

    // Check ulintf->llintf list
    for(link = intf_llintfs(ul); link; link = intf_link_next(link)) {
        if(!strcmp(intf_name(intf_link_current(link)), llintf)) {
            found = true;
        }
    }
    if(should_be_linked) {
        when_false(found, out)
    } else {
        when_true(found, out)
    }

    // Check llintf->ulintf list
    found = false;
    for(link = intf_ulintfs(ll); link; link = intf_link_next(link)) {
        if(!strcmp(intf_name(intf_link_current(link)), ulintf)) {
            found = true;
        }
    }
    if(should_be_linked) {
        when_false(found, out)
    } else {
        when_true(found, out)
    }

out_unlinked:
    ret = true;

out:
    return ret;
}

static bool is_linked(const char* ulintf, const char* llintf) {
    return check_link(ulintf, llintf, true);
}

static bool is_unlinked(const char* ulintf, const char* llintf) {
    return check_link(ulintf, llintf, false);
}

static void modify_instance(const char* template_format,
                            const char* template_name,
                            const char* instance,
                            bool add) {
    amxd_object_t* obj = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    obj = amxd_dm_findf(&dm, template_format, template_name);
    assert_non_null(obj);

    amxd_trans_select_object(&trans, obj);
    if(add) {
        amxd_trans_add_inst(&trans, 0, instance);
    } else {
        amxd_trans_del_inst(&trans, 0, instance);
    }
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    handle_events();

    amxd_trans_clean(&trans);
}

static void del_instance(const char* template_format, const char* template_name, const char* instance) {
    modify_instance(template_format, template_name, instance, false);
}

static void test_link_event_handler(UNUSED const char* const sig_name,
                                    UNUSED const amxc_var_t* const data,
                                    void* const priv) {
    int* counter = (int*) priv;
    (*counter)++;
}

void test_linked_after_linkintfs_call(UNUSED void** state) {
    const char* ulintf = "intf_0";
    const char* llintf = "intf_1";

    link_intfs(ulintf, llintf);
    assert_true(is_linked(ulintf, llintf));
}

void test_unlinked_after_unlinkintfs_call(UNUSED void** state) {
    const char* ulintf = "intf_0";
    const char* llintf = "intf_1";

    link_intfs(ulintf, llintf);
    assert_true(is_linked(ulintf, llintf));

    unlink_intfs(ulintf, llintf);
    assert_true(is_unlinked(ulintf, llintf));
}

void test_unlinked_after_intf_removed(UNUSED void** state) {
    const char* ulintf = "intf_0";
    const char* llintf = "intf_1";

    link_intfs(ulintf, llintf);
    assert_true(is_linked(ulintf, llintf));

    del_instance("NetModel.Intf.", "", llintf);
    assert_true(is_unlinked(ulintf, llintf));
}

void test_linked_events(UNUSED void** state) {
    const char* ulintf = "intf_0";
    const char* llintf = "intf_1";
    int link_count = 0;
    int unlink_count = 0;

    amxp_slot_connect(&dm.sigmngr, "nm:link", NULL, test_link_event_handler, &link_count);
    amxp_slot_connect(&dm.sigmngr, "nm:unlink", NULL, test_link_event_handler, &unlink_count);

    link_intfs(ulintf, llintf);
    handle_events();
    assert_int_equal(link_count, 1);

    unlink_intfs(ulintf, llintf);
    handle_events();
    assert_int_equal(unlink_count, 1);
}

void test_link_unlink_toggle(UNUSED void** state) {
    const char* ulintf = "intf_0";
    const char* llintf = "intf_1";

    link_intfs(ulintf, llintf);
    unlink_intfs(ulintf, llintf);
    handle_events();
}

void test_new_intf_from_odl(UNUSED void** state) {
    amxc_var_t params;
    amxd_object_t* obj = NULL;
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    intf_t* intf = NULL;

    assert_non_null(root_obj);
    amxc_var_init(&params);

    // Load intf from odl
    assert_int_equal(amxo_parser_parse_file(&parser, "../../odl/netmodel_defaults/00_netmodel_defaults.odl", root_obj), 0);

    // Verify the interface creation
    obj = amxd_dm_findf(&dm, "%s", "NetModel.Intf.ethLink-link_lo.");
    assert_non_null(obj);
    intf = intf_find("ethLink-link_lo");
    assert_non_null(intf);

    // Verify Flags in the datamodel
    assert_int_equal(amxd_object_get_params(obj, &params, amxd_dm_access_private), 0);
    assert_int_equal(strcmp(GET_CHAR(&params, "Flags"), "netdev"), 0);

    // Verify that flag set is empty
    assert_int_equal(amxc_set_get_count(intf_flagset(intf), NULL), 0);

    handle_events();

    // Flag set must not be empty
    assert_true(amxc_set_has_flag(intf_flagset(intf), "netdev"));

    amxc_var_clean(&params);
}

void test_status_param_sync(UNUSED void** state) {
    amxc_var_t params;
    amxd_object_t* obj = amxd_dm_findf(&dm, "%s", "NetModel.Intf.intf_0.");
    amxd_trans_t trans;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    assert_non_null(obj);

    // Get parameters
    assert_int_equal(amxd_object_get_params(obj, &params, amxd_dm_access_private), 0);
    assert_int_equal(strcmp(GET_CHAR(&params, "Status_ext"), "Unknown"), 0);
    assert_false(GET_BOOL(&params, "Status"));

    // Set Status_ext to "Up"
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status_ext", "Up");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    // Status should be true now
    assert_int_equal(amxd_object_get_params(obj, &params, amxd_dm_access_private), 0);
    assert_int_equal(strcmp(GET_CHAR(&params, "Status_ext"), "Up"), 0);
    assert_true(GET_BOOL(&params, "Status"));

    // Set Status_ext to "Down"
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status_ext", "Down");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    // Status should be false again
    assert_int_equal(amxd_object_get_params(obj, &params, amxd_dm_access_private), 0);
    assert_int_equal(strcmp(GET_CHAR(&params, "Status_ext"), "Down"), 0);
    assert_false(GET_BOOL(&params, "Status"));

    amxd_trans_clean(&trans);
    amxc_var_clean(&params);
}
