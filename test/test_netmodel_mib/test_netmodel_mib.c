/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <amxo/amxo.h>

#include "intf.h"
#include "dm_intf.h"

#include "test_netmodel_mib.h"
#include "test_utility.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "../../odl/netmodel_definition.odl";
static const char* mib_dir = "./";

static const int nr_of_intfs = 1;

int test_mib_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    int retval = 0;
    amxd_trans_t trans;
    int i = 0;
    char name[64];

    assert_int_equal(amxd_trans_init(&trans), 0);

    test_init_dm(&dm, &parser);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    retval = amxo_parser_parse_file(&parser, odl_defs, root_obj);
    printf("PARSER MESSAGE = %s\n", amxc_string_get(&parser.msg, 0));
    assert_int_equal(retval, 0);

    handle_events();

    assert_int_equal(_nm_main(0, &dm, &parser), 0);

    amxd_trans_select_pathf(&trans, "NetModel.Intf.");

    for(i = 0; i < nr_of_intfs; i++) {
        snprintf(name, sizeof(name), "intf_%d", i);
        amxd_trans_add_inst(&trans, 0, name);
        amxd_trans_select_pathf(&trans, ".^");
    }

    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    handle_events();

    amxd_trans_clean(&trans);
    return 0;
}

int test_mib_teardown(UNUSED void** state) {
    assert_int_equal(_nm_main(1, &dm, &parser), 0);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_mib_loaded_after_flag_added(UNUSED void** state) {
    amxd_object_t* nm_obj = amxd_dm_findf(&dm, "NetModel.");
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    const char* mib_name = "testmib";
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    // Load mib dir
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "path", mib_dir);
    assert_int_equal(amxd_object_invoke_function(nm_obj, "scanMibDir", &args, &ret), 0);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    handle_events();

    // Add flag
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", mib_name);
    assert_int_equal(amxd_object_invoke_function(intf_obj, "setFlag", &args, &ret), 0);
    handle_events();

    // Check if mib is added
    assert_true(amxd_object_has_mib(intf_obj, mib_name));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_mib_unloaded_after_flag_removed(UNUSED void** state) {
    amxd_object_t* intf_obj = amxd_dm_findf(&dm, "NetModel.Intf.intf_0");
    const char* mib_name = "testmib";
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    // Check if mib is added
    assert_true(amxd_object_has_mib(intf_obj, mib_name));

    // remove flag
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", mib_name);
    assert_int_equal(amxd_object_invoke_function(intf_obj, "clearFlag", &args, &ret), 0);
    handle_events();

    // Check if mib is removed
    assert_false(amxd_object_has_mib(intf_obj, mib_name));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
